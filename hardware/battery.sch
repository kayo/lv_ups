EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:stm32
LIBS:addon
LIBS:project-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 2 6
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L INDUCTOR_SMALL L201
U 1 1 57FFE096
P 3750 3600
AR Path="/57FFE043/57FFE096" Ref="L201"  Part="1" 
AR Path="/5800D9F5/57FFE096" Ref="L301"  Part="1" 
AR Path="/5800E773/57FFE096" Ref="L401"  Part="1" 
AR Path="/5800E781/57FFE096" Ref="L501"  Part="1" 
F 0 "L201" H 3750 3700 50  0000 C CNN
F 1 "40u 1A" H 3750 3550 50  0000 C CNN
F 2 "" H 3750 3600 50  0001 C CNN
F 3 "" H 3750 3600 50  0000 C CNN
	1    3750 3600
	1    0    0    -1  
$EndComp
$Comp
L CP C201
U 1 1 57FFE0D7
P 4100 3850
AR Path="/57FFE043/57FFE0D7" Ref="C201"  Part="1" 
AR Path="/5800D9F5/57FFE0D7" Ref="C301"  Part="1" 
AR Path="/5800E773/57FFE0D7" Ref="C401"  Part="1" 
AR Path="/5800E781/57FFE0D7" Ref="C501"  Part="1" 
F 0 "C201" H 4125 3950 50  0000 L CNN
F 1 "100u" H 4125 3750 50  0000 L CNN
F 2 "Local:c_elec_6.3x7.7" H 4138 3700 50  0001 C CNN
F 3 "" H 4100 3850 50  0000 C CNN
	1    4100 3850
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR03
U 1 1 57FFE14B
P 4100 4100
AR Path="/57FFE043/57FFE14B" Ref="#PWR03"  Part="1" 
AR Path="/5800D9F5/57FFE14B" Ref="#PWR015"  Part="1" 
AR Path="/5800E773/57FFE14B" Ref="#PWR027"  Part="1" 
AR Path="/5800E781/57FFE14B" Ref="#PWR039"  Part="1" 
F 0 "#PWR03" H 4100 3850 50  0001 C CNN
F 1 "GND" H 4100 3950 50  0000 C CNN
F 2 "" H 4100 4100 50  0000 C CNN
F 3 "" H 4100 4100 50  0000 C CNN
	1    4100 4100
	1    0    0    -1  
$EndComp
$Comp
L Battery BT201
U 1 1 57FFE202
P 4800 3850
AR Path="/57FFE043/57FFE202" Ref="BT201"  Part="1" 
AR Path="/5800D9F5/57FFE202" Ref="BT301"  Part="1" 
AR Path="/5800E773/57FFE202" Ref="BT401"  Part="1" 
AR Path="/5800E781/57FFE202" Ref="BT501"  Part="1" 
AR Path="/57FFE202" Ref="BT201"  Part="1" 
F 0 "BT201" H 4900 3900 50  0000 L CNN
F 1 "BAT" H 4900 3800 50  0000 L CNN
F 2 "" V 4800 3890 50  0001 C CNN
F 3 "" V 4800 3890 50  0000 C CNN
	1    4800 3850
	1    0    0    -1  
$EndComp
$Comp
L R RS201
U 1 1 57FFE26B
P 4800 4350
AR Path="/57FFE043/57FFE26B" Ref="RS201"  Part="1" 
AR Path="/5800D9F5/57FFE26B" Ref="RS301"  Part="1" 
AR Path="/5800E773/57FFE26B" Ref="RS401"  Part="1" 
AR Path="/5800E781/57FFE26B" Ref="RS501"  Part="1" 
F 0 "RS201" V 4880 4350 50  0000 C CNN
F 1 "R05" V 4800 4350 50  0000 C CNN
F 2 "Local:R_0805" V 4730 4350 50  0001 C CNN
F 3 "" H 4800 4350 50  0000 C CNN
	1    4800 4350
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR04
U 1 1 57FFE46E
P 4800 4600
AR Path="/57FFE043/57FFE46E" Ref="#PWR04"  Part="1" 
AR Path="/5800D9F5/57FFE46E" Ref="#PWR016"  Part="1" 
AR Path="/5800E773/57FFE46E" Ref="#PWR028"  Part="1" 
AR Path="/5800E781/57FFE46E" Ref="#PWR040"  Part="1" 
F 0 "#PWR04" H 4800 4350 50  0001 C CNN
F 1 "GND" H 4800 4450 50  0000 C CNN
F 2 "" H 4800 4600 50  0000 C CNN
F 3 "" H 4800 4600 50  0000 C CNN
	1    4800 4600
	1    0    0    -1  
$EndComp
$Comp
L D_Schottky D201
U 1 1 57FFE48F
P 3400 3850
AR Path="/57FFE043/57FFE48F" Ref="D201"  Part="1" 
AR Path="/5800D9F5/57FFE48F" Ref="D301"  Part="1" 
AR Path="/5800E773/57FFE48F" Ref="D401"  Part="1" 
AR Path="/5800E781/57FFE48F" Ref="D501"  Part="1" 
F 0 "D201" H 3400 3950 50  0000 C CNN
F 1 "SS14" H 3400 3750 50  0000 C CNN
F 2 "Local:SMA_Standard" H 3400 3850 50  0001 C CNN
F 3 "" H 3400 3850 50  0000 C CNN
	1    3400 3850
	0    -1   1    0   
$EndComp
$Comp
L GND #PWR05
U 1 1 57FFE50C
P 3400 4100
AR Path="/57FFE043/57FFE50C" Ref="#PWR05"  Part="1" 
AR Path="/5800D9F5/57FFE50C" Ref="#PWR017"  Part="1" 
AR Path="/5800E773/57FFE50C" Ref="#PWR029"  Part="1" 
AR Path="/5800E781/57FFE50C" Ref="#PWR041"  Part="1" 
F 0 "#PWR05" H 3400 3850 50  0001 C CNN
F 1 "GND" H 3400 3950 50  0000 C CNN
F 2 "" H 3400 4100 50  0000 C CNN
F 3 "" H 3400 4100 50  0000 C CNN
	1    3400 4100
	1    0    0    -1  
$EndComp
Text Notes 8000 6400 0    47   ~ 0
/* Current amplifier */\n\n/* U[P] - powering voltage */\n/* U[S] - voltage on current shunt */\n/* I[S] - current through shunt */\n/* R[S] - resistance of current shunt */\n/* R[F] - far resistor of input divider */\n/* R[N] - near resistor of input divider */\n/* U[B] - voltage from input divider */\n\nEQ[U[S]]: U[S]=I[S]*R[S]$\nEQ[ID]: (U[P]-U[B])/(U[P]-U[S])=R[F]/(R[F]+R[N])$\nEQ[U[B]]: solve(EQ[ID], U[B])[1], EQ[U[S]];\nEQ[R[N]]: solve(EQ[ID], R[N])[1], EQ[U[S]];\n\nPR[IN]: [U[P]=3.3, R[S]=0.05]$\nPR[MAX]: [I[S]=1.2, U[B]=1.3]$\nPR[MIN]: [I[S]=-3.2, U[B]=0.05]$\nPR[R[F]]: R[F]=33e3$\nEQ[R[N]], PR[IN], PR[R[F]], PR[MIN];\nPR[R[N]]: R[N]=2.2e3$\nEQ[U[B]], PR[IN], PR[R[F]], PR[R[N]], I[S]=-3.2;\nEQ[U[B]], PR[IN], PR[R[F]], PR[R[N]], I[S]=1.2;\n\n/* U[A] - ADC input voltage */\n/* I[A] - ADC step current */\n/* B[A] - ADC bits */\n/* R[U] - pullup resistor */\n/* R[D] - pulldown resistor */\n/* R[B] - feedback resistor */\n\nEQ[U[A]]: U[A] = U[B]*(1+R[B]/R[D]+R[B]/R[U])-U[P]*R[B]/R[U], EQ[U[B]]$\nEQ[U[A(MIN)]]: EQ[U[A]], I[S]=I[S(MIN)], U[A]=U[A(MIN)]$\nEQ[U[A(MAX)]]: EQ[U[A]], I[S]=I[S(MAX)], U[A]=U[A(MAX)]$\nEQ[U[A(RANGE)]]: [EQ[U[A(MIN)]], EQ[U[A(MAX)]]];\nEQ[I[S(RANGE)]]: solve(EQ[U[A(RANGE)]], [I[S(MIN)], I[S(MAX)]])[1];\nEQ[R[UD]]: solve(EQ[U[A(RANGE)]], [R[U], R[D]])[1];\nEQ[I[A]]: I[A] = (I[S(MAX)]-I[S(MIN)])*U[P]/((U[A(MAX)]-U[A(MIN)])*2^B[A]);\nEQ[ADC[RANGE]]: [ADC[MIN]=U[A(MIN)]*2^B[A]/U[P], ADC[MAX]=U[A(MAX)]*2^B[A]/U[P]]$\n\nPR[IN]: [U[P]=3.3, R[S]=0.05, R[N]=2.2e3, R[F]=33e3, B[A]=12]$\nPR[I[S(RANGE)]]: [I[S(MIN)]=-3.2, I[S(MAX)]=1.2]$\nPR[U[A(RANGE)]]: [U[A(MIN)]=0.05, U[A(MAX)]=1.3]$\n\nPR[R[B]]: R[B]=8.2e3$\nEQ[R[UD]], PR[IN], PR[R[B]], PR[I[S(RANGE)]], PR[U[A(RANGE)]];\nPR[R[UD]]: [R[U]=100e3, R[D]=1.5e3]$\nEQ[U[A(RANGE)]], PR[IN], PR[I[S(RANGE)]], PR[R[UD]], PR[R[B]];\nPR[I[S(RANGE_REAL)]]: EQ[I[S(RANGE)]], PR[IN], PR[U[A(RANGE)]], PR[R[UD]], PR[R[B]];\nEQ[I[A]], PR[IN], PR[U[A(RANGE)]], PR[I[S(RANGE_REAL)]];\n\nPR[R[B]]: R[B]=4.7e3$\nEQ[R[UD]], PR[IN], PR[R[B]], PR[I[S(RANGE)]], PR[U[A(RANGE)]];\nPR[R[UD]]: [R[U]=68e3, R[D]=1e3]$\nEQ[U[A(RANGE)]], PR[IN], PR[I[S(RANGE)]], PR[R[UD]], PR[R[B]];\nPR[I[S(RANGE(OUT))]]: EQ[I[S(RANGE)]], PR[IN], PR[U[A(RANGE)]], PR[R[UD]], PR[R[B]];\nEQ[I[A]], PR[IN], PR[U[A(RANGE)]], PR[I[S(RANGE(OUT))]];\nEQ[ADC[RANGE]], PR[IN], PR[U[A(RANGE)]];
Text HLabel 10400 1400 2    47   Input ~ 0
I_BAT
Text Notes 5400 4450 0    47   ~ 0
/* Voltage measurement */\n\n/* U[S] - voltage measurement step */\n/* U[MAX] - maximum ADC voltage */\n/* ADC[B] - number of ADC bits */\n/* R[L] - low resistor of divider */\n/* R[H] - high resistor of divider */\n\nEQ[U[OUT]]: U[OUT] = U[IN] * R[L] / (R[L] + R[H]);\nEQ[R[H]]: solve(EQ[U[OUT]], R[H])[1], U[IN]=U[MAX], U[OUT]=U[ADC];\nEQ[U[MAX]]: solve(EQ[R[H]], U[MAX])[1];\nEQ[U[S]]: U[S] = U[MAX] / (2^ADC[B] - 1), U[IN]=U[MAX];\n\nPR[IN]: [ADC[B]=12, U[ADC]=3.3, R[L]=3.3e3]$\n\nEQ[R[H]], PR[IN], U[MAX]=5.5;\nPR[R[H]]: R[H]=2.2e3$\nPR[U[MAX]]: EQ[U[MAX]], PR[IN], PR[R[H]];\nEQ[U[S]], PR[IN], PR[R[H]], PR[U[MAX]];
Text Notes 6900 1200 0    47   ~ 0
Measurement Ranges\n\nU: 0(0)...5.5V(4096), 1343uV\n(min...max, step)\n\nI: -3.37(62)...1.25A(1614), 2.979uA\n(discharge_min...charge_max, step)
$Comp
L R R202
U 1 1 57FFF2A5
P 5800 2550
AR Path="/57FFE043/57FFF2A5" Ref="R202"  Part="1" 
AR Path="/5800D9F5/57FFF2A5" Ref="R302"  Part="1" 
AR Path="/5800E773/57FFF2A5" Ref="R402"  Part="1" 
AR Path="/5800E781/57FFF2A5" Ref="R502"  Part="1" 
F 0 "R202" V 5880 2550 50  0000 C CNN
F 1 "3K3" V 5800 2550 50  0000 C CNN
F 2 "Local:R_0603" V 5730 2550 50  0001 C CNN
F 3 "" H 5800 2550 50  0000 C CNN
	1    5800 2550
	-1   0    0    -1  
$EndComp
$Comp
L R R203
U 1 1 57FFF2F6
P 5800 2050
AR Path="/57FFE043/57FFF2F6" Ref="R203"  Part="1" 
AR Path="/5800D9F5/57FFF2F6" Ref="R303"  Part="1" 
AR Path="/5800E773/57FFF2F6" Ref="R403"  Part="1" 
AR Path="/5800E781/57FFF2F6" Ref="R503"  Part="1" 
F 0 "R203" V 5880 2050 50  0000 C CNN
F 1 "2K2" V 5800 2050 50  0000 C CNN
F 2 "Local:R_0603" V 5730 2050 50  0001 C CNN
F 3 "" H 5800 2050 50  0000 C CNN
	1    5800 2050
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR06
U 1 1 57FFF739
P 5800 2800
AR Path="/57FFE043/57FFF739" Ref="#PWR06"  Part="1" 
AR Path="/5800D9F5/57FFF739" Ref="#PWR018"  Part="1" 
AR Path="/5800E773/57FFF739" Ref="#PWR030"  Part="1" 
AR Path="/5800E781/57FFF739" Ref="#PWR042"  Part="1" 
F 0 "#PWR06" H 5800 2550 50  0001 C CNN
F 1 "GND" H 5800 2650 50  0000 C CNN
F 2 "" H 5800 2800 50  0000 C CNN
F 3 "" H 5800 2800 50  0000 C CNN
	1    5800 2800
	1    0    0    -1  
$EndComp
Text HLabel 6600 2300 2    47   Input ~ 0
U_BAT
Text Notes 1600 5750 0    47   ~ 0
/* R[ja] - junction to ambient termal resistance */\n\nEQ[T[P]]: T[j] - T[a] = P * R[ja];\nEQ[T[j]]: solve(EQ[T[P]], T[j])[1];\n\nPR[IN]: [T[a]=25, P=1.1*0.094*1^2]$\nPR[SI2306]: [R[ja]=100, T[j]=50]$\n\nEQ[T[j]], PR[IN], PR[SI2306];
Text Notes 600  2950 0    47   ~ 0
/* Stepdown calculation */\n/* R[C] - out capacitor ESR */\n/* P[D] - back diode power */\n\nEQ[L]: L = (U[IN(MAX)] - U[OUT]) * U[OUT] / (U[IN(MAX)] * f * I[RIPPLE])$\nEQ[I[RIPPLE]]: solve(EQ[L], I[RIPPLE])[1]$\nEQ[I[PEAK]]: I[PEAK] = I[OUT(MAX)] + I[RIPPLE]/2$\nEQ[C]: C = 1.2 * L * (I[OUT(MAX)] + I[RIPPLE]/2)^2 / ((dU[OUT] + U[OUT])^2 - U[OUT]^2)$\nEQ[U[RIPPLE]]: U[RIPPLE] = (U[IN(MAX)] - U[OUT]) * U[OUT]^2 / (2 * C * L * U[IN(MAX)]^2 * f^2) + I[RIPPLE] * R[C]$\nEQ[R[C]]: solve(EQ[U[RIPPLE]], R[C])[1]$\n\nEQ[T[P]]: T[J] - T[A] = P * R[JA]$\nEQ[T[J]]: solve(EQ[T[P]], T[J])[1]$\nEQ[P[D]]: P[D] = (1 - U[OUT]/U[IN(MAX)]) * I[OUT(MAX)] * U[D]$\nEQ[P[T]]: P[T] = U[OUT]/U[IN(MIN)] * I[OUT(MAX)]^2 * R[DS(ON)] + C[RSS] * U[IN(MAX)] * f * I[OUT(MAX)] / I[GATE]$\n\nPR[IN]: [T[A]=25, U[IN(MIN)]=4.5, U[IN(MAX)]=6, U[OUT]=4.35, dU[OUT]=0.05, U[RIPPLE]=0.04, I[OUT(MAX)]=1, f=100e3]$\n\nPR[I[RIPPLE(TARGET)]]: I[RIPPLE]=I[OUT(MAX)]*0.2, PR[IN];\nEQ[L], PR[I[RIPPLE(TARGET)]], PR[IN];\nPR[L]: L = 60e-6$\nPR[I[RIPPLE]]: EQ[I[RIPPLE]], PR[IN], PR[L];\nEQ[I[PEAK]], PR[IN], PR[I[RIPPLE]];\nEQ[C], PR[IN], PR[L], PR[I[RIPPLE]];\nPR[C]: C = 100e-6$\nEQ[R[C]], PR[IN], PR[L], PR[C], PR[I[RIPPLE]];\n\nPR[SS14]: [U[D]=0.5, R[JA]=88]$\nPR[AO3407]: [C[RSS]=65e-12, I[GATE]=10e-3, R[DS(ON)]=1.3*87e-3, R[JA]=125]$\nEQ[P[D]], PR[IN], PR[SS14];\nEQ[T[J]], PR[IN], PR[SS14], P=0.14;\nEQ[P[T]], PR[IN], PR[AO3407];\nEQ[T[J]], PR[IN], PR[AO3407], P=0.11;
$Comp
L Q_PMOS_GSD Q201
U 1 1 5800B125
P 3100 3700
AR Path="/57FFE043/5800B125" Ref="Q201"  Part="1" 
AR Path="/5800D9F5/5800B125" Ref="Q301"  Part="1" 
AR Path="/5800E773/5800B125" Ref="Q401"  Part="1" 
AR Path="/5800E781/5800B125" Ref="Q501"  Part="1" 
F 0 "Q201" V 3450 3700 50  0000 C CNN
F 1 "AO3407" V 3350 3700 50  0000 C CNN
F 2 "Local:SOT-23" H 3300 3800 50  0001 C CNN
F 3 "" H 3100 3700 50  0000 C CNN
	1    3100 3700
	0    1    -1   0   
$EndComp
$Comp
L R R201
U 1 1 5800B203
P 1450 4500
AR Path="/57FFE043/5800B203" Ref="R201"  Part="1" 
AR Path="/5800D9F5/5800B203" Ref="R301"  Part="1" 
AR Path="/5800E773/5800B203" Ref="R401"  Part="1" 
AR Path="/5800E781/5800B203" Ref="R501"  Part="1" 
F 0 "R201" V 1530 4500 50  0000 C CNN
F 1 "K33" V 1450 4500 50  0000 C CNN
F 2 "Local:R_0603" V 1380 4500 50  0001 C CNN
F 3 "" H 1450 4500 50  0000 C CNN
	1    1450 4500
	0    1    -1   0   
$EndComp
Text GLabel 2800 3500 1    47   Input ~ 0
PWR_IN
Text HLabel 1200 4500 0    47   Input ~ 0
S_BAT
$Comp
L R R204
U 1 1 5800B88D
P 5300 7000
AR Path="/57FFE043/5800B88D" Ref="R204"  Part="1" 
AR Path="/5800D9F5/5800B88D" Ref="R304"  Part="1" 
AR Path="/5800E773/5800B88D" Ref="R404"  Part="1" 
AR Path="/5800E781/5800B88D" Ref="R504"  Part="1" 
F 0 "R204" V 5380 7000 50  0000 C CNN
F 1 "4K7" V 5300 7000 50  0000 C CNN
F 2 "Local:R_0603" V 5230 7000 50  0001 C CNN
F 3 "" H 5300 7000 50  0000 C CNN
	1    5300 7000
	-1   0    0    1   
$EndComp
$Comp
L THERMISTOR TH201
U 1 1 5800B8EB
P 5300 6400
AR Path="/57FFE043/5800B8EB" Ref="TH201"  Part="1" 
AR Path="/5800D9F5/5800B8EB" Ref="TH301"  Part="1" 
AR Path="/5800E773/5800B8EB" Ref="TH401"  Part="1" 
AR Path="/5800E781/5800B8EB" Ref="TH501"  Part="1" 
F 0 "TH201" V 5400 6450 50  0000 C CNN
F 1 "100K" V 5200 6400 50  0000 C BNN
F 2 "Local:THERM_RES" H 5300 6400 50  0001 C CNN
F 3 "" H 5300 6400 50  0000 C CNN
	1    5300 6400
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR07
U 1 1 5800BBEE
P 5300 7250
AR Path="/57FFE043/5800BBEE" Ref="#PWR07"  Part="1" 
AR Path="/5800D9F5/5800BBEE" Ref="#PWR019"  Part="1" 
AR Path="/5800E773/5800BBEE" Ref="#PWR031"  Part="1" 
AR Path="/5800E781/5800BBEE" Ref="#PWR043"  Part="1" 
F 0 "#PWR07" H 5300 7000 50  0001 C CNN
F 1 "GND" H 5300 7100 50  0000 C CNN
F 2 "" H 5300 7250 50  0000 C CNN
F 3 "" H 5300 7250 50  0000 C CNN
	1    5300 7250
	1    0    0    -1  
$EndComp
Text GLabel 5300 6050 1    47   Input ~ 0
+3V3A
$Comp
L C C202
U 1 1 5800BE35
P 5800 7000
AR Path="/57FFE043/5800BE35" Ref="C202"  Part="1" 
AR Path="/5800D9F5/5800BE35" Ref="C302"  Part="1" 
AR Path="/5800E773/5800BE35" Ref="C402"  Part="1" 
AR Path="/5800E781/5800BE35" Ref="C502"  Part="1" 
F 0 "C202" H 5825 7100 50  0000 L CNN
F 1 "10n" H 5825 6900 50  0000 L CNN
F 2 "Local:C_0603" H 5838 6850 50  0001 C CNN
F 3 "" H 5800 7000 50  0000 C CNN
	1    5800 7000
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR08
U 1 1 5800BED8
P 5800 7250
AR Path="/57FFE043/5800BED8" Ref="#PWR08"  Part="1" 
AR Path="/5800D9F5/5800BED8" Ref="#PWR020"  Part="1" 
AR Path="/5800E773/5800BED8" Ref="#PWR032"  Part="1" 
AR Path="/5800E781/5800BED8" Ref="#PWR044"  Part="1" 
F 0 "#PWR08" H 5800 7000 50  0001 C CNN
F 1 "GND" H 5800 7100 50  0000 C CNN
F 2 "" H 5800 7250 50  0000 C CNN
F 3 "" H 5800 7250 50  0000 C CNN
	1    5800 7250
	1    0    0    -1  
$EndComp
Text HLabel 5900 6750 2    47   Input ~ 0
T_BAT
Text Notes 650  3750 0    47   ~ 0
Charger circuit\n\nf=200kHz\nI=1A\nC=100uF\nL=30uH
Text Notes 5550 1350 0    47   ~ 0
/* Measurement filter */\n\nEQ[f]: f = 1 / (2 * %pi * R * C);\nEQ[R]: solve(EQ[f], R)[1];\n\nPR[IN]: [f=10e3, C=10e-9];\nEQ[R], PR[IN], numer;\nPR[R]: R=1.5e3;\nEQ[f], PR[IN], PR[R], numer;
$Comp
L R R205
U 1 1 58012FF2
P 10050 1400
AR Path="/57FFE043/58012FF2" Ref="R205"  Part="1" 
AR Path="/5800D9F5/58012FF2" Ref="R305"  Part="1" 
AR Path="/5800E773/58012FF2" Ref="R405"  Part="1" 
AR Path="/5800E781/58012FF2" Ref="R505"  Part="1" 
F 0 "R205" V 10130 1400 50  0000 C CNN
F 1 "1K5" V 10050 1400 50  0000 C CNN
F 2 "Local:R_0603" V 9980 1400 50  0001 C CNN
F 3 "" H 10050 1400 50  0000 C CNN
	1    10050 1400
	0    1    -1   0   
$EndComp
$Comp
L C C203
U 1 1 58013191
P 10300 1650
AR Path="/57FFE043/58013191" Ref="C203"  Part="1" 
AR Path="/5800D9F5/58013191" Ref="C303"  Part="1" 
AR Path="/5800E773/58013191" Ref="C403"  Part="1" 
AR Path="/5800E781/58013191" Ref="C503"  Part="1" 
F 0 "C203" H 10325 1750 50  0000 L CNN
F 1 "10n" H 10325 1550 50  0000 L CNN
F 2 "Local:C_0603" H 10338 1500 50  0001 C CNN
F 3 "" H 10300 1650 50  0000 C CNN
	1    10300 1650
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR021
U 1 1 580132AF
P 10300 1900
AR Path="/5800D9F5/580132AF" Ref="#PWR021"  Part="1" 
AR Path="/5800E773/580132AF" Ref="#PWR033"  Part="1" 
AR Path="/5800E781/580132AF" Ref="#PWR045"  Part="1" 
AR Path="/57FFE043/580132AF" Ref="#PWR09"  Part="1" 
F 0 "#PWR09" H 10300 1650 50  0001 C CNN
F 1 "GND" H 10300 1750 50  0000 C CNN
F 2 "" H 10300 1900 50  0000 C CNN
F 3 "" H 10300 1900 50  0000 C CNN
	1    10300 1900
	1    0    0    -1  
$EndComp
$Comp
L R R206
U 1 1 580135B4
P 6250 2300
AR Path="/57FFE043/580135B4" Ref="R206"  Part="1" 
AR Path="/5800D9F5/580135B4" Ref="R306"  Part="1" 
AR Path="/5800E773/580135B4" Ref="R406"  Part="1" 
AR Path="/5800E781/580135B4" Ref="R506"  Part="1" 
F 0 "R206" V 6330 2300 50  0000 C CNN
F 1 "1K5" V 6250 2300 50  0000 C CNN
F 2 "Local:R_0603" V 6180 2300 50  0001 C CNN
F 3 "" H 6250 2300 50  0000 C CNN
	1    6250 2300
	0    1    -1   0   
$EndComp
$Comp
L C C204
U 1 1 58013674
P 6500 2550
AR Path="/57FFE043/58013674" Ref="C204"  Part="1" 
AR Path="/5800D9F5/58013674" Ref="C304"  Part="1" 
AR Path="/5800E773/58013674" Ref="C404"  Part="1" 
AR Path="/5800E781/58013674" Ref="C504"  Part="1" 
F 0 "C204" H 6525 2650 50  0000 L CNN
F 1 "10n" H 6525 2450 50  0000 L CNN
F 2 "Local:C_0603" H 6538 2400 50  0001 C CNN
F 3 "" H 6500 2550 50  0000 C CNN
	1    6500 2550
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR022
U 1 1 5801367A
P 6500 2800
AR Path="/5800D9F5/5801367A" Ref="#PWR022"  Part="1" 
AR Path="/5800E773/5801367A" Ref="#PWR034"  Part="1" 
AR Path="/5800E781/5801367A" Ref="#PWR046"  Part="1" 
AR Path="/57FFE043/5801367A" Ref="#PWR010"  Part="1" 
F 0 "#PWR010" H 6500 2550 50  0001 C CNN
F 1 "GND" H 6500 2650 50  0000 C CNN
F 2 "" H 6500 2800 50  0000 C CNN
F 3 "" H 6500 2800 50  0000 C CNN
	1    6500 2800
	1    0    0    -1  
$EndComp
Text Label 4300 3600 2    47   ~ 0
+BAT
Text Label 4600 3600 0    47   ~ 0
+BAT
Text Label 5600 1800 0    47   ~ 0
+BAT
$Comp
L Q_PMOS_GSD Q202
U 1 1 5801B424
P 3900 6200
AR Path="/57FFE043/5801B424" Ref="Q202"  Part="1" 
AR Path="/5800D9F5/5801B424" Ref="Q302"  Part="1" 
AR Path="/5800E773/5801B424" Ref="Q402"  Part="1" 
AR Path="/5800E781/5801B424" Ref="Q502"  Part="1" 
F 0 "Q202" V 4250 6200 50  0000 C CNN
F 1 "AO3407" V 4150 6200 50  0000 C CNN
F 2 "Local:SOT-23" H 4100 6300 50  0001 C CNN
F 3 "" H 3900 6200 50  0000 C CNN
	1    3900 6200
	0    -1   -1   0   
$EndComp
Text Label 3400 6100 0    47   ~ 0
+BAT
Text GLabel 4200 6100 2    47   Input ~ 0
PWR_OUT
$Comp
L R R207
U 1 1 5801BFB9
P 3600 6350
AR Path="/57FFE043/5801BFB9" Ref="R207"  Part="1" 
AR Path="/5800D9F5/5801BFB9" Ref="R307"  Part="1" 
AR Path="/5800E773/5801BFB9" Ref="R407"  Part="1" 
AR Path="/5800E781/5801BFB9" Ref="R507"  Part="1" 
F 0 "R207" V 3680 6350 50  0000 C CNN
F 1 "5K6" V 3600 6350 50  0000 C CNN
F 2 "Local:R_0603" V 3530 6350 50  0001 C CNN
F 3 "" H 3600 6350 50  0000 C CNN
	1    3600 6350
	-1   0    0    -1  
$EndComp
$Comp
L R R208
U 1 1 5801C26A
P 3900 6850
AR Path="/57FFE043/5801C26A" Ref="R208"  Part="1" 
AR Path="/5800D9F5/5801C26A" Ref="R308"  Part="1" 
AR Path="/5800E773/5801C26A" Ref="R408"  Part="1" 
AR Path="/5800E781/5801C26A" Ref="R508"  Part="1" 
F 0 "R208" V 3980 6850 50  0000 C CNN
F 1 "K56" V 3900 6850 50  0000 C CNN
F 2 "Local:R_0603" V 3830 6850 50  0001 C CNN
F 3 "" H 3900 6850 50  0000 C CNN
	1    3900 6850
	-1   0    0    1   
$EndComp
Text HLabel 3700 7100 0    47   Input ~ 0
E_BAT
$Comp
L Q_NPN_BEC Q203
U 1 1 5801DBCC
P 2000 3900
AR Path="/57FFE043/5801DBCC" Ref="Q203"  Part="1" 
AR Path="/5800D9F5/5801DBCC" Ref="Q303"  Part="1" 
AR Path="/5800E773/5801DBCC" Ref="Q403"  Part="1" 
AR Path="/5800E781/5801DBCC" Ref="Q503"  Part="1" 
F 0 "Q203" H 2200 3950 50  0000 L CNN
F 1 "SS9013" H 2200 3850 50  0000 L CNN
F 2 "Local:SOT-23" H 2200 4000 50  0001 C CNN
F 3 "" H 2000 3900 50  0000 C CNN
	1    2000 3900
	1    0    0    -1  
$EndComp
$Comp
L R R209
U 1 1 5801E9B3
P 1700 3650
AR Path="/57FFE043/5801E9B3" Ref="R209"  Part="1" 
AR Path="/5800D9F5/5801E9B3" Ref="R309"  Part="1" 
AR Path="/5800E773/5801E9B3" Ref="R409"  Part="1" 
AR Path="/5800E781/5801E9B3" Ref="R509"  Part="1" 
F 0 "R209" V 1780 3650 50  0000 C CNN
F 1 "1K" V 1700 3650 50  0000 C CNN
F 2 "Local:R_0603" V 1630 3650 50  0001 C CNN
F 3 "" H 1700 3650 50  0000 C CNN
	1    1700 3650
	-1   0    0    -1  
$EndComp
Text Notes 950  4750 0    47   ~ 0
PWM from uC\napprox. 10mA
Text Notes 2750 3600 1    47   ~ 0
External Supply
Text Notes 4150 6000 0    47   ~ 0
Output Supply
Text Notes 3450 7250 0    47   ~ 0
Enable Battery
Text Notes 9150 650  0    47   ~ 0
Current Sense
Text Notes 5950 1900 0    47   ~ 0
Voltage Sense
Text Notes 3200 3300 0    47   ~ 0
Battery Charger
Text Notes 5500 6250 0    47   ~ 0
Temperature Sense
$Comp
L Q_PNP_BEC Q204
U 1 1 58028168
P 2000 4500
AR Path="/57FFE043/58028168" Ref="Q204"  Part="1" 
AR Path="/5800D9F5/58028168" Ref="Q304"  Part="1" 
AR Path="/5800E773/58028168" Ref="Q404"  Part="1" 
AR Path="/5800E781/58028168" Ref="Q504"  Part="1" 
F 0 "Q204" H 2200 4550 50  0000 L CNN
F 1 "SS9012" H 2200 4450 50  0000 L CNN
F 2 "Local:SOT-23" H 2200 4600 50  0001 C CNN
F 3 "" H 2000 4500 50  0000 C CNN
	1    2000 4500
	1    0    0    1   
$EndComp
$Comp
L GND #PWR011
U 1 1 58028708
P 2100 4800
AR Path="/57FFE043/58028708" Ref="#PWR011"  Part="1" 
AR Path="/5800D9F5/58028708" Ref="#PWR023"  Part="1" 
AR Path="/5800E773/58028708" Ref="#PWR035"  Part="1" 
AR Path="/5800E781/58028708" Ref="#PWR047"  Part="1" 
F 0 "#PWR011" H 2100 4550 50  0001 C CNN
F 1 "GND" H 2100 4650 50  0000 C CNN
F 2 "" H 2100 4800 50  0000 C CNN
F 3 "" H 2100 4800 50  0000 C CNN
	1    2100 4800
	1    0    0    -1  
$EndComp
$Comp
L LM324N U201
U 1 1 58036F84
P 9400 1400
AR Path="/57FFE043/58036F84" Ref="U201"  Part="1" 
AR Path="/5800D9F5/58036F84" Ref="U201"  Part="2" 
AR Path="/5800E773/58036F84" Ref="U201"  Part="3" 
AR Path="/5800E781/58036F84" Ref="U201"  Part="4" 
F 0 "U201" H 9450 1600 50  0000 C CNN
F 1 "LM324N" H 9500 1200 50  0000 C CNN
F 2 "Local:SOIC-16_3.9x9.9mm_Pitch1.27mm" H 9350 1500 50  0001 C CNN
F 3 "" H 9450 1600 50  0000 C CNN
	1    9400 1400
	1    0    0    -1  
$EndComp
$Comp
L GND #PWR013
U 1 1 5803761D
P 9300 2000
AR Path="/57FFE043/5803761D" Ref="#PWR013"  Part="1" 
AR Path="/5800D9F5/5803761D" Ref="#PWR025"  Part="1" 
AR Path="/5800E773/5803761D" Ref="#PWR037"  Part="1" 
AR Path="/5800E781/5803761D" Ref="#PWR049"  Part="1" 
F 0 "#PWR013" H 9300 1750 50  0001 C CNN
F 1 "GND" H 9300 1850 50  0000 C CNN
F 2 "" H 9300 2000 50  0000 C CNN
F 3 "" H 9300 2000 50  0000 C CNN
	1    9300 2000
	1    0    0    -1  
$EndComp
$Comp
L R R211
U 1 1 5803793A
P 9550 1800
AR Path="/57FFE043/5803793A" Ref="R211"  Part="1" 
AR Path="/5800D9F5/5803793A" Ref="R311"  Part="1" 
AR Path="/5800E773/5803793A" Ref="R411"  Part="1" 
AR Path="/5800E781/5803793A" Ref="R511"  Part="1" 
F 0 "R211" V 9630 1800 50  0000 C CNN
F 1 "39K" V 9550 1800 50  0000 C CNN
F 2 "Local:R_0603" V 9480 1800 50  0001 C CNN
F 3 "" H 9550 1800 50  0000 C CNN
	1    9550 1800
	0    1    -1   0   
$EndComp
$Comp
L R R210
U 1 1 580387FC
P 8800 1750
AR Path="/57FFE043/580387FC" Ref="R210"  Part="1" 
AR Path="/5800D9F5/580387FC" Ref="R310"  Part="1" 
AR Path="/5800E773/580387FC" Ref="R410"  Part="1" 
AR Path="/5800E781/580387FC" Ref="R510"  Part="1" 
F 0 "R210" V 8880 1750 50  0000 C CNN
F 1 "2K2" V 8800 1750 50  0000 C CNN
F 2 "Local:R_0603" V 8730 1750 50  0001 C CNN
F 3 "" H 8800 1750 50  0000 C CNN
	1    8800 1750
	-1   0    0    -1  
$EndComp
$Comp
L GND #PWR014
U 1 1 58038B6B
P 8800 2000
AR Path="/57FFE043/58038B6B" Ref="#PWR014"  Part="1" 
AR Path="/5800D9F5/58038B6B" Ref="#PWR026"  Part="1" 
AR Path="/5800E773/58038B6B" Ref="#PWR038"  Part="1" 
AR Path="/5800E781/58038B6B" Ref="#PWR050"  Part="1" 
F 0 "#PWR014" H 8800 1750 50  0001 C CNN
F 1 "GND" H 8800 1850 50  0000 C CNN
F 2 "" H 8800 2000 50  0000 C CNN
F 3 "" H 8800 2000 50  0000 C CNN
	1    8800 2000
	1    0    0    -1  
$EndComp
Text Label 2900 4200 0    47   ~ 0
GATE
Text Label 2300 4200 2    47   ~ 0
GATE
Text GLabel 1600 3400 0    47   Input ~ 0
PWR_IN
Text Label 6050 2300 2    47   ~ 0
U_SIG
Text Label 5050 4100 2    47   ~ 0
I_SIG
Text GLabel 9300 1000 1    47   Input ~ 0
+3V3A
$Comp
L R R212
U 1 1 58078A78
P 9000 1050
AR Path="/57FFE043/58078A78" Ref="R212"  Part="1" 
AR Path="/5800D9F5/58078A78" Ref="R312"  Part="1" 
AR Path="/5800E773/58078A78" Ref="R412"  Part="1" 
AR Path="/5800E781/58078A78" Ref="R512"  Part="1" 
F 0 "R212" V 9080 1050 50  0000 C CNN
F 1 "39K" V 9000 1050 50  0000 C CNN
F 2 "Local:R_0603" V 8930 1050 50  0001 C CNN
F 3 "" H 9000 1050 50  0000 C CNN
	1    9000 1050
	-1   0    0    -1  
$EndComp
Text GLabel 9000 800  1    47   Input ~ 0
+3V3A
$Comp
L R R213
U 1 1 5807B8D4
P 8600 1550
AR Path="/57FFE043/5807B8D4" Ref="R213"  Part="1" 
AR Path="/5800D9F5/5807B8D4" Ref="R313"  Part="1" 
AR Path="/5800E773/5807B8D4" Ref="R413"  Part="1" 
AR Path="/5800E781/5807B8D4" Ref="R513"  Part="1" 
F 0 "R213" V 8680 1550 50  0000 C CNN
F 1 "2K2" V 8600 1550 50  0000 C CNN
F 2 "Local:R_0603" V 8530 1550 50  0001 C CNN
F 3 "" H 8600 1550 50  0000 C CNN
	1    8600 1550
	-1   0    0    -1  
$EndComp
Text Label 8600 1950 1    47   ~ 0
I_SIG_L
$Comp
L R R214
U 1 1 5807BC7F
P 8600 1050
AR Path="/57FFE043/5807BC7F" Ref="R214"  Part="1" 
AR Path="/5800D9F5/5807BC7F" Ref="R314"  Part="1" 
AR Path="/5800E773/5807BC7F" Ref="R414"  Part="1" 
AR Path="/5800E781/5807BC7F" Ref="R514"  Part="1" 
F 0 "R214" V 8680 1050 50  0000 C CNN
F 1 "39K" V 8600 1050 50  0000 C CNN
F 2 "Local:R_0603" V 8530 1050 50  0001 C CNN
F 3 "" H 8600 1050 50  0000 C CNN
	1    8600 1050
	-1   0    0    -1  
$EndComp
Text GLabel 8600 800  1    47   Input ~ 0
+3V3A
Text Label 8850 1300 2    47   ~ 0
I_SIG
Wire Wire Line
	4600 3600 4800 3600
Wire Wire Line
	4100 3600 4100 3700
Connection ~ 4100 3600
Wire Wire Line
	4100 4000 4100 4100
Wire Wire Line
	4800 3600 4800 3700
Wire Wire Line
	4800 4000 4800 4200
Wire Wire Line
	4800 4500 4800 4600
Wire Wire Line
	3300 3600 3500 3600
Wire Wire Line
	3400 3600 3400 3700
Wire Wire Line
	3400 4000 3400 4100
Wire Wire Line
	4800 4100 5050 4100
Connection ~ 4800 4100
Wire Wire Line
	5800 2200 5800 2400
Wire Wire Line
	5800 2300 6100 2300
Connection ~ 5800 2300
Wire Wire Line
	5800 2700 5800 2800
Connection ~ 3400 3600
Wire Wire Line
	5300 6650 5300 6850
Wire Wire Line
	5300 6150 5300 6050
Wire Wire Line
	5300 6750 5900 6750
Connection ~ 5300 6750
Wire Wire Line
	5300 7150 5300 7250
Wire Wire Line
	5800 6750 5800 6850
Wire Wire Line
	5800 7150 5800 7250
Connection ~ 5800 6750
Wire Wire Line
	10200 1400 10400 1400
Wire Wire Line
	10300 1400 10300 1500
Connection ~ 10300 1400
Wire Wire Line
	10300 1800 10300 1900
Wire Wire Line
	6500 2700 6500 2800
Wire Wire Line
	6400 2300 6600 2300
Wire Wire Line
	6500 2300 6500 2400
Connection ~ 6500 2300
Wire Wire Line
	4000 3600 4300 3600
Wire Wire Line
	5600 1800 5800 1800
Wire Wire Line
	5800 1800 5800 1900
Wire Wire Line
	1600 3400 2100 3400
Wire Wire Line
	3400 6100 3700 6100
Wire Wire Line
	4100 6100 4200 6100
Wire Wire Line
	3900 6400 3900 6700
Wire Wire Line
	3600 6100 3600 6200
Connection ~ 3600 6100
Wire Wire Line
	3600 6500 3600 6600
Wire Wire Line
	3600 6600 3900 6600
Connection ~ 3900 6600
Wire Wire Line
	3900 7000 3900 7100
Wire Wire Line
	3900 7100 3700 7100
Wire Wire Line
	2100 4100 2100 4300
Wire Wire Line
	3100 4200 3100 3900
Wire Wire Line
	1800 3900 1700 3900
Wire Wire Line
	1700 3800 1700 4500
Connection ~ 1700 3900
Wire Wire Line
	1700 3500 1700 3400
Connection ~ 1700 3400
Wire Wire Line
	1300 4500 1200 4500
Connection ~ 2100 4200
Wire Wire Line
	1600 4500 1800 4500
Connection ~ 1700 4500
Wire Wire Line
	2100 4700 2100 4800
Wire Wire Line
	8800 1500 9100 1500
Wire Wire Line
	9300 1700 9300 2000
Wire Wire Line
	9300 1100 9300 1000
Wire Wire Line
	9700 1400 9900 1400
Wire Wire Line
	9800 1300 9800 1800
Connection ~ 9800 1400
Wire Wire Line
	8800 1900 8800 2000
Wire Wire Line
	2800 3500 2800 3600
Wire Wire Line
	2800 3600 2900 3600
Wire Wire Line
	2100 3400 2100 3700
Wire Wire Line
	3100 4200 2900 4200
Wire Wire Line
	2100 4200 2300 4200
Wire Wire Line
	9000 900  9000 800 
Wire Wire Line
	9800 1800 9700 1800
Wire Wire Line
	9000 1800 9400 1800
Connection ~ 9000 1500
Wire Wire Line
	9100 1300 8600 1300
Wire Wire Line
	8600 1700 8600 1950
Wire Wire Line
	8600 1200 8600 1400
Connection ~ 8600 1300
Wire Wire Line
	8600 900  8600 800 
Wire Wire Line
	9000 1200 9000 1800
Wire Wire Line
	8800 1500 8800 1600
Text Notes 5900 2600 0    47   ~ 0
R[L]
Text Notes 5900 2100 0    47   ~ 0
R[H]
Text Notes 4900 4350 0    47   ~ 0
R[S]
Text Notes 8450 1100 2    47   ~ 0
R[F]
Text Notes 8300 1600 0    47   ~ 0
R[N]
Text Notes 8700 1100 0    47   ~ 0
R[U]
Text Notes 8850 1950 0    47   ~ 0
R[D]
Text Notes 9500 1950 0    47   ~ 0
R[B]
Text Notes 12700 3100 0    47   ~ 0
/* Input offset divider */\n/* U[P] - powering voltage */\n/* U[S] - voltage on current shunt */\n/* I[S] - current through shunt */\n/* R[S] - resistance of current shunt */\n/* R[F] - far resistor of input divider */\n/* R[N] - near resistor of input divider */\n/* U[B] - voltage from input divider */\n\nEQ[U[S]]: U[S]=I[S]*R[S];\nEQ[ID]: (U[P]-U[B])/(U[P]-U[S])=R[F]/(R[F]+R[N]);\nEQ[U[B]]: solve(EQ[ID], U[B])[1], EQ[U[S]];\nEQ[R[N]]: solve(EQ[ID], R[N])[1], EQ[U[S]];\nEQ[I[S]]: solve(EQ[U[B]], I[S])[1];\n\nPR[IN]: [U[P]=3.3, R[S]=0.27]$\nPR[MAX]: [I[S]=1.2, U[B]=1.3]$\nPR[MIN]: [I[S]=-3.2, U[B]=0.05]$\nPR[R[F]]: R[F]=12e3$\nEQ[R[N]], PR[IN], PR[R[F]], PR[MIN];\nPR[R[N]]: R[N]=3.3e3$\nPR[U[B(MIN)]]: EQ[U[B]], PR[IN], PR[R[F]], PR[R[N]], I[S]=-3.2;\nPR[U[B(MAX)]]: EQ[U[B]], PR[IN], PR[R[F]], PR[R[N]], I[S]=1.2;\nEQ[I[S]], PR[IN], PR[R[F]], PR[R[N]], PR[U[B(MIN)]];\nEQ[I[S]], PR[IN], PR[R[F]], PR[R[N]], PR[U[B(MAX)]];
Text Label 9800 750  3    47   ~ 0
I_SIG
$Comp
L R R215
U 1 1 5807FB4E
P 9800 1150
AR Path="/57FFE043/5807FB4E" Ref="R215"  Part="1" 
AR Path="/5800D9F5/5807FB4E" Ref="R315"  Part="1" 
AR Path="/5800E773/5807FB4E" Ref="R415"  Part="1" 
AR Path="/5800E781/5807FB4E" Ref="R515"  Part="1" 
F 0 "R215" V 9880 1150 50  0000 C CNN
F 1 "NC" V 9800 1150 50  0000 C CNN
F 2 "Local:R_0603" V 9730 1150 50  0001 C CNN
F 3 "" H 9800 1150 50  0000 C CNN
	1    9800 1150
	-1   0    0    -1  
$EndComp
Wire Wire Line
	9800 750  9800 1000
Text Notes 9950 1250 1    47   ~ 0
Passthrough
$EndSCHEMATC
