EESchema Schematic File Version 2
LIBS:spice
LIBS:project-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L V V1
U 1 1 572C2687
P 8800 3700
F 0 "V1" H 8700 3900 60  0000 C CNN
F 1 "DC 0V PWL 0mS -32V 10mS 12V" V 9050 3700 47  0000 C CNN
F 2 "" H 8800 3700 60  0000 C CNN
F 3 "" H 8800 3700 60  0000 C CNN
	1    8800 3700
	1    0    0    -1  
$EndComp
$Comp
L 0 #GND01
U 1 1 572C3390
P 8800 4100
F 0 "#GND01" H 8800 4000 40  0001 C CNN
F 1 "0" H 8800 4030 40  0000 C CNN
F 2 "" H 8800 4100 60  0000 C CNN
F 3 "" H 8800 4100 60  0000 C CNN
	1    8800 4100
	1    0    0    -1  
$EndComp
$Comp
L R RB1
U 1 1 5801E066
P 10600 4300
F 0 "RB1" H 10600 4400 60  0000 C CNN
F 1 "10" H 10600 4200 60  0000 C CNN
F 2 "" H 10600 4300 60  0000 C CNN
F 3 "" H 10600 4300 60  0000 C CNN
	1    10600 4300
	0    -1   -1   0   
$EndComp
$Comp
L 0 #GND02
U 1 1 5801E24A
P 10600 5300
F 0 "#GND02" H 10600 5200 40  0001 C CNN
F 1 "0" H 10600 5230 40  0000 C CNN
F 2 "" H 10600 5300 60  0000 C CNN
F 3 "" H 10600 5300 60  0000 C CNN
	1    10600 5300
	1    0    0    -1  
$EndComp
Text Notes 6600 1050 0    60   ~ 0
-pspice\n* \n.include ../.spice/amp.lib
Text GLabel 10600 3300 1    60   Input ~ 0
PWR
$Comp
L R RS1
U 1 1 58025071
P 10600 4900
F 0 "RS1" H 10600 5000 60  0000 C CNN
F 1 "{Rsh}" H 10600 4800 60  0000 C CNN
F 2 "" H 10600 4900 60  0000 C CNN
F 3 "" H 10600 4900 60  0000 C CNN
	1    10600 4900
	0    -1   -1   0   
$EndComp
Text GLabel 10600 4600 2    60   Input ~ 0
BSH
Text Notes 7150 2050 0    60   ~ 0
-pspice\n* \n.param\n+ Rsh=0.05
Text GLabel 8800 3300 1    60   Input ~ 0
PWR
$Comp
L OA XO1
U 1 1 5804D4D8
P 2300 1600
F 0 "XO1" H 2400 1750 60  0000 C CNN
F 1 "LM224_ON" H 2300 900 60  0000 C CNN
F 2 "" H 2200 1600 60  0000 C CNN
F 3 "" H 2200 1600 60  0000 C CNN
	1    2300 1600
	1    0    0    -1  
$EndComp
Text GLabel 1100 2200 3    60   Input ~ 0
BSH
$Comp
L 0 #GND03
U 1 1 5804DA67
P 2300 2100
F 0 "#GND03" H 2300 2000 40  0001 C CNN
F 1 "0" H 2300 2030 40  0000 C CNN
F 2 "" H 2300 2100 60  0000 C CNN
F 3 "" H 2300 2100 60  0000 C CNN
	1    2300 2100
	1    0    0    -1  
$EndComp
Text GLabel 2300 1100 1    60   Input ~ 0
PWA
$Comp
L R R13
U 1 1 5804DC8B
P 3200 2000
F 0 "R13" H 3200 2100 60  0000 C CNN
F 1 "47K" H 3200 1900 60  0000 C CNN
F 2 "" H 3200 2000 60  0000 C CNN
F 3 "" H 3200 2000 60  0000 C CNN
	1    3200 2000
	0    -1   -1   0   
$EndComp
$Comp
L 0 #GND04
U 1 1 5804DE39
P 3200 2400
F 0 "#GND04" H 3200 2300 40  0001 C CNN
F 1 "0" H 3200 2330 40  0000 C CNN
F 2 "" H 3200 2400 60  0000 C CNN
F 3 "" H 3200 2400 60  0000 C CNN
	1    3200 2400
	1    0    0    -1  
$EndComp
$Comp
L R R12
U 1 1 5804E039
P 2800 2000
F 0 "R12" H 2800 2100 60  0000 C CNN
F 1 "47K" H 2800 1900 60  0000 C CNN
F 2 "" H 2800 2000 60  0000 C CNN
F 3 "" H 2800 2000 60  0000 C CNN
	1    2800 2000
	0    -1   -1   0   
$EndComp
$Comp
L R R11
U 1 1 5804E1AC
P 1400 2000
F 0 "R11" H 1400 2100 60  0000 C CNN
F 1 "12K" H 1400 1900 60  0000 C CNN
F 2 "" H 1400 2000 60  0000 C CNN
F 3 "" H 1400 2000 60  0000 C CNN
	1    1400 2000
	0    -1   -1   0   
$EndComp
$Comp
L 0 #GND05
U 1 1 5804E3C4
P 1400 2400
F 0 "#GND05" H 1400 2300 40  0001 C CNN
F 1 "0" H 1400 2330 40  0000 C CNN
F 2 "" H 1400 2400 60  0000 C CNN
F 3 "" H 1400 2400 60  0000 C CNN
	1    1400 2400
	1    0    0    -1  
$EndComp
Text GLabel 3400 1600 2    60   Input ~ 0
IS1
Text Notes 600  7200 0    60   ~ 0
/* Current amplifier */\n\n/* U[P] - powering voltage */\n/* U[S] - voltage on current shunt */\n/* I[S] - current through shunt */\n/* R[S] - resistance of current shunt */\n/* R[F] - far resistor of input divider */\n/* R[N] - near resistor of input divider */\n/* U[B] - voltage from input divider */\n\nEQ[U[S]]: U[S]=I[S]*R[S];\nEQ[ID]: (U[P]-U[B])/(U[P]-U[S])=R[F]/(R[F]+R[N]);\nEQ[U[B]]: solve(EQ[ID], U[B])[1], EQ[U[S]];\nEQ[R[N]]: solve(EQ[ID], R[N])[1], EQ[U[S]];\n\nPR[IN]: [U[P]=3.3, R[S]=0.05]$\nPR[MAX]: [I[S]=1.2, U[B]=1.3]$\nPR[MIN]: [I[S]=-3.2, U[B]=0.05]$\nPR[R[F]]: R[F]=33e3$\nEQ[R[N]], PR[IN], PR[R[F]], PR[MIN];\nPR[R[N]]: R[N]=2.2e3$\nEQ[U[B]], PR[IN], PR[R[F]], PR[R[N]], I[S]=-3.2;\nEQ[U[B]], PR[IN], PR[R[F]], PR[R[N]], I[S]=1.2;\n\n/* U[A] - ADC input voltage */\n/* I[A] - ADC step current */\n/* B[A] - ADC bits */\n/* R[D] - pulldown resistor */\n/* R[B] - feedback resistor */\n\nEQ[U[A]]: U[A]=U[B]*(1+R[B]/R[D]), EQ[U[B]];\nEQ[R[B]]: solve(EQ[U[A]], R[B])[1];\nEQ[I[S]]: solve(EQ[U[A]], I[S])[1];\nEQ[I[S(MIN)]]: EQ[I[S]], U[A]=U[A(MIN)], I[S]=I[S(MIN)]$\nEQ[I[S(MAX)]]: EQ[I[S]], U[A]=U[A(MAX)], I[S]=I[S(MAX)]$\nEQ[I[S(RANGE)]]: [EQ[I[S(MIN)]], EQ[I[S(MAX)]]]$\nEQ[U[A(RANGE)]]: solve(EQ[I[S(RANGE)]], [U[A(MIN)], U[A(MAX)]])[1]$\nEQ[I[A]]: I[A] = (I[S(MAX)]-I[S(MIN)])*U[P]/((U[A(MAX)]-U[A(MIN)])*2^B[A])$\n\nPR[IN]: [U[P]=3.3, R[S]=0.05, R[N]=2.2e3, R[F]=33e3, B[A]=12]$\nPR[I[S(RANGE)]]: [I[S(MIN)]=-3.2, I[S(MAX)]=1.2]$\nPR[U[A(RANGE)]]: [U[A(MIN)]=0.5, U[A(MAX)]=1.3]$\nPR[R[D]]: R[D]=12e3$\nEQ[R[B]], PR[IN], PR[R[D]], PR[I[S(MAX)]], PR[U[A(MAX)]];\nPR[R[B]]: R[B]=47e3$\nEQ[U[A(RANGE)]], PR[IN], PR[I[S(RANGE)]], PR[R[D]], PR[R[B]];\nPR[R[DB]]: [PR[R[D]], PR[R[B]]]$\n\nPR[U[A(RANGE)]]: [U[A(MIN)]=0.05, U[A(MAX)]=1.3]$\nPR[I[S(RANGE(OUT))]]: EQ[I[S(RANGE)]], PR[IN], PR[R[DB]], PR[U[A(RANGE)]];\nEQ[I[A]], PR[IN], PR[U[A(RANGE)]], PR[I[S(RANGE(OUT))]];
Text Notes 10850 4450 1    60   ~ 0
BATTERY
Text Notes 8800 6300 0    60   ~ 0
+pspice\n* \n.control\n  option nopage\n  tran 100nS 10mS 0mS\n  let Ibt = vs1#branch\n  set gnuplot_terminal=png\n  gnuplot project\n  + Ibt\n  + v(bsh)\n  + v(bid)\n  + v(is1)\n  + v(is2)\n  meas tran Vbid_min min v(bid)\n  meas tran Vbid_max max v(bid)\n  meas tran Vis1_min min v(is1)\n  meas tran Vis1_max max v(is1)\n  meas tran Vis2_min min v(is2)\n  meas tran Vis2_max max v(is2)\n.endc
Wire Wire Line
	8800 3900 8800 4100
Wire Wire Line
	10600 3900 10600 4100
Wire Wire Line
	10600 5100 10600 5300
Wire Wire Line
	10600 4500 10600 4700
Wire Wire Line
	8800 3300 8800 3500
Wire Wire Line
	1100 1500 2000 1500
Wire Wire Line
	1400 1700 2000 1700
Wire Wire Line
	2300 1900 2300 2100
Wire Wire Line
	2300 1300 2300 1100
Wire Wire Line
	2600 1600 3400 1600
Wire Wire Line
	3200 1600 3200 1800
Wire Wire Line
	3200 2200 3200 2400
Wire Wire Line
	2800 1600 2800 1800
Connection ~ 2800 1600
Wire Wire Line
	2800 2200 2800 2400
Wire Wire Line
	2800 2400 1800 2400
Wire Wire Line
	1800 2400 1800 1700
Wire Wire Line
	1400 1700 1400 1800
Connection ~ 1800 1700
Wire Wire Line
	1400 2200 1400 2400
Connection ~ 3200 1600
$Comp
L V V2
U 1 1 5806AC7F
P 9700 3700
F 0 "V2" H 9600 3900 60  0000 C CNN
F 1 "DC 3.3V" H 9850 3500 47  0000 C CNN
F 2 "" H 9700 3700 60  0000 C CNN
F 3 "" H 9700 3700 60  0000 C CNN
	1    9700 3700
	1    0    0    -1  
$EndComp
$Comp
L 0 #GND06
U 1 1 5806AC85
P 9700 4100
F 0 "#GND06" H 9700 4000 40  0001 C CNN
F 1 "0" H 9700 4030 40  0000 C CNN
F 2 "" H 9700 4100 60  0000 C CNN
F 3 "" H 9700 4100 60  0000 C CNN
	1    9700 4100
	1    0    0    -1  
$EndComp
Text GLabel 9700 3300 1    60   Input ~ 0
PWA
Wire Wire Line
	9700 3900 9700 4100
Wire Wire Line
	9700 3300 9700 3500
$Comp
L V VS1
U 1 1 5806B0AC
P 10600 3700
F 0 "VS1" H 10500 3900 60  0000 C CNN
F 1 "DC 0V" H 10750 3500 47  0000 C CNN
F 2 "" H 10600 3700 60  0000 C CNN
F 3 "" H 10600 3700 60  0000 C CNN
	1    10600 3700
	1    0    0    -1  
$EndComp
Wire Wire Line
	10600 3300 10600 3500
$Comp
L OA XO2
U 1 1 5806BB4F
P 5500 1600
F 0 "XO2" H 5600 1750 60  0000 C CNN
F 1 "LM224_ON" H 5500 900 60  0000 C CNN
F 2 "" H 5400 1600 60  0000 C CNN
F 3 "" H 5400 1600 60  0000 C CNN
	1    5500 1600
	1    0    0    -1  
$EndComp
Text GLabel 4300 2200 3    60   Input ~ 0
BSH
$Comp
L 0 #GND07
U 1 1 5806BB56
P 5500 2100
F 0 "#GND07" H 5500 2000 40  0001 C CNN
F 1 "0" H 5500 2030 40  0000 C CNN
F 2 "" H 5500 2100 60  0000 C CNN
F 3 "" H 5500 2100 60  0000 C CNN
	1    5500 2100
	1    0    0    -1  
$EndComp
Text GLabel 5500 1100 1    60   Input ~ 0
PWA
$Comp
L R R24
U 1 1 5806BB5D
P 6400 2000
F 0 "R24" H 6400 2100 60  0000 C CNN
F 1 "47K" H 6400 1900 60  0000 C CNN
F 2 "" H 6400 2000 60  0000 C CNN
F 3 "" H 6400 2000 60  0000 C CNN
	1    6400 2000
	0    -1   -1   0   
$EndComp
$Comp
L 0 #GND08
U 1 1 5806BB63
P 6400 2400
F 0 "#GND08" H 6400 2300 40  0001 C CNN
F 1 "0" H 6400 2330 40  0000 C CNN
F 2 "" H 6400 2400 60  0000 C CNN
F 3 "" H 6400 2400 60  0000 C CNN
	1    6400 2400
	1    0    0    -1  
$EndComp
$Comp
L R R23
U 1 1 5806BB69
P 6000 2000
F 0 "R23" H 6000 2100 60  0000 C CNN
F 1 "4.7K" H 6000 1900 60  0000 C CNN
F 2 "" H 6000 2000 60  0000 C CNN
F 3 "" H 6000 2000 60  0000 C CNN
	1    6000 2000
	0    -1   -1   0   
$EndComp
$Comp
L R R22
U 1 1 5806BB6F
P 4600 2000
F 0 "R22" H 4600 2100 60  0000 C CNN
F 1 "1K" H 4600 1900 60  0000 C CNN
F 2 "" H 4600 2000 60  0000 C CNN
F 3 "" H 4600 2000 60  0000 C CNN
	1    4600 2000
	0    -1   -1   0   
$EndComp
$Comp
L 0 #GND09
U 1 1 5806BB75
P 4600 2400
F 0 "#GND09" H 4600 2300 40  0001 C CNN
F 1 "0" H 4600 2330 40  0000 C CNN
F 2 "" H 4600 2400 60  0000 C CNN
F 3 "" H 4600 2400 60  0000 C CNN
	1    4600 2400
	1    0    0    -1  
$EndComp
Text GLabel 6600 1600 2    60   Input ~ 0
IS2
Wire Wire Line
	4300 1500 5200 1500
Wire Wire Line
	4600 1700 5200 1700
Wire Wire Line
	5500 1900 5500 2100
Wire Wire Line
	5500 1300 5500 1100
Wire Wire Line
	5800 1600 6600 1600
Wire Wire Line
	6400 1600 6400 1800
Wire Wire Line
	6400 2200 6400 2400
Wire Wire Line
	6000 1600 6000 1800
Connection ~ 6000 1600
Wire Wire Line
	6000 2200 6000 2400
Wire Wire Line
	6000 2400 5000 2400
Wire Wire Line
	5000 2400 5000 1700
Wire Wire Line
	4600 1400 4600 1800
Connection ~ 5000 1700
Wire Wire Line
	4600 2200 4600 2400
Connection ~ 6400 1600
Text Notes 1400 750  0    60   ~ 0
Version 1. Simple Non-Inverting Amplifier
Text Notes 4900 750  0    60   ~ 0
Version 2. Scaling Amplifier
$Comp
L R R21
U 1 1 5806BC52
P 4600 1200
F 0 "R21" H 4600 1300 60  0000 C CNN
F 1 "68K" H 4600 1100 60  0000 C CNN
F 2 "" H 4600 1200 60  0000 C CNN
F 3 "" H 4600 1200 60  0000 C CNN
	1    4600 1200
	0    -1   -1   0   
$EndComp
Wire Wire Line
	4600 1000 4600 800 
Text GLabel 4600 800  1    60   Input ~ 0
PWA
Connection ~ 4600 1700
Text Notes 4300 7750 0    60   ~ 0
/* Current amplifier */\n\n/* U[P] - powering voltage */\n/* U[S] - voltage on current shunt */\n/* I[S] - current through shunt */\n/* R[S] - resistance of current shunt */\n/* R[F] - far resistor of input divider */\n/* R[N] - near resistor of input divider */\n/* U[B] - voltage from input divider */\n\nEQ[U[S]]: U[S]=I[S]*R[S]$\nEQ[ID]: (U[P]-U[B])/(U[P]-U[S])=R[F]/(R[F]+R[N])$\nEQ[U[B]]: solve(EQ[ID], U[B])[1], EQ[U[S]];\nEQ[R[N]]: solve(EQ[ID], R[N])[1], EQ[U[S]];\n\nPR[IN]: [U[P]=3.3, R[S]=0.05]$\nPR[MAX]: [I[S]=1.2, U[B]=1.3]$\nPR[MIN]: [I[S]=-3.2, U[B]=0.05]$\nPR[R[F]]: R[F]=33e3$\nEQ[R[N]], PR[IN], PR[R[F]], PR[MIN];\nPR[R[N]]: R[N]=2.2e3$\nEQ[U[B]], PR[IN], PR[R[F]], PR[R[N]], I[S]=-3.2;\nEQ[U[B]], PR[IN], PR[R[F]], PR[R[N]], I[S]=1.2;\n\n/* U[A] - ADC input voltage */\n/* I[A] - ADC step current */\n/* B[A] - ADC bits */\n/* R[U] - pullup resistor */\n/* R[D] - pulldown resistor */\n/* R[B] - feedback resistor */\n\nEQ[U[A]]: U[A] = U[B]*(1+R[B]/R[D]+R[B]/R[U])-U[P]*R[B]/R[U], EQ[U[B]]$\nEQ[U[A(MIN)]]: EQ[U[A]], I[S]=I[S(MIN)], U[A]=U[A(MIN)]$\nEQ[U[A(MAX)]]: EQ[U[A]], I[S]=I[S(MAX)], U[A]=U[A(MAX)]$\nEQ[U[A(RANGE)]]: [EQ[U[A(MIN)]], EQ[U[A(MAX)]]];\nEQ[I[S(RANGE)]]: solve(EQ[U[A(RANGE)]], [I[S(MIN)], I[S(MAX)]])[1];\nEQ[R[UD]]: solve(EQ[U[A(RANGE)]], [R[U], R[D]])[1];\nEQ[I[A]]: I[A] = (I[S(MAX)]-I[S(MIN)])*U[P]/((U[A(MAX)]-U[A(MIN)])*2^B[A]);\nEQ[ADC[RANGE]]: [ADC[MIN]=U[A(MIN)]*2^B[A]/U[P], ADC[MAX]=U[A(MAX)]*2^B[A]/U[P]]$\n\nPR[IN]: [U[P]=3.3, R[S]=0.05, R[N]=2.2e3, R[F]=33e3, B[A]=12]$\nPR[I[S(RANGE)]]: [I[S(MIN)]=-3.2, I[S(MAX)]=1.2]$\nPR[U[A(RANGE)]]: [U[A(MIN)]=0.05, U[A(MAX)]=1.3]$\n\nPR[R[B]]: R[B]=8.2e3$\nEQ[R[UD]], PR[IN], PR[R[B]], PR[I[S(RANGE)]], PR[U[A(RANGE)]];\nPR[R[UD]]: [R[U]=100e3, R[D]=1.5e3]$\nEQ[U[A(RANGE)]], PR[IN], PR[I[S(RANGE)]], PR[R[UD]], PR[R[B]];\nPR[I[S(RANGE_REAL)]]: EQ[I[S(RANGE)]], PR[IN], PR[U[A(RANGE)]], PR[R[UD]], PR[R[B]];\nEQ[I[A]], PR[IN], PR[U[A(RANGE)]], PR[I[S(RANGE_REAL)]];\n\nPR[R[B]]: R[B]=4.7e3$\nEQ[R[UD]], PR[IN], PR[R[B]], PR[I[S(RANGE)]], PR[U[A(RANGE)]];\nPR[R[UD]]: [R[U]=68e3, R[D]=1e3]$\nEQ[U[A(RANGE)]], PR[IN], PR[I[S(RANGE)]], PR[R[UD]], PR[R[B]];\nPR[I[S(RANGE(OUT))]]: EQ[I[S(RANGE)]], PR[IN], PR[U[A(RANGE)]], PR[R[UD]], PR[R[B]];\nEQ[I[A]], PR[IN], PR[U[A(RANGE)]], PR[I[S(RANGE(OUT))]];\nEQ[ADC[RANGE]], PR[IN], PR[U[A(RANGE)]];
Text Notes 8200 2750 0    60   ~ 0
/* Input offset divider */\n/* U[P] - powering voltage */\n/* U[S] - voltage on current shunt */\n/* I[S] - current through shunt */\n/* R[S] - resistance of current shunt */\n/* R[F] - far resistor of input divider */\n/* R[N] - near resistor of input divider */\n/* U[B] - voltage from input divider */\n\nEQ[U[S]]: U[S]=I[S]*R[S];\nEQ[ID]: (U[P]-U[B])/(U[P]-U[S])=R[F]/(R[F]+R[N]);\nEQ[U[B]]: solve(EQ[ID], U[B])[1], EQ[U[S]];\nEQ[R[N]]: solve(EQ[ID], R[N])[1], EQ[U[S]];\n\nPR[IN]: [U[P]=3.3, R[S]=0.05]$\nPR[MAX]: [I[S]=1.2, U[B]=1.3]$\nPR[MIN]: [I[S]=-3.2, U[B]=0.05]$\nPR[R[F]]: R[F]=33e3$\nEQ[R[N]], PR[IN], PR[R[F]], PR[MIN];\nPR[R[N]]: R[N]=2.2e3$\nEQ[U[B]], PR[IN], PR[R[F]], PR[R[N]], I[S]=-3.2;\nEQ[U[B]], PR[IN], PR[R[F]], PR[R[N]], I[S]=1.2;
$Comp
L R RI12
U 1 1 58071557
P 1100 1800
F 0 "RI12" H 1100 1900 60  0000 C CNN
F 1 "2.2K" H 1100 1700 60  0000 C CNN
F 2 "" H 1100 1800 60  0000 C CNN
F 3 "" H 1100 1800 60  0000 C CNN
	1    1100 1800
	0    -1   -1   0   
$EndComp
$Comp
L R RI11
U 1 1 580715FB
P 1100 1200
F 0 "RI11" H 1100 1300 60  0000 C CNN
F 1 "33K" H 1100 1100 60  0000 C CNN
F 2 "" H 1100 1200 60  0000 C CNN
F 3 "" H 1100 1200 60  0000 C CNN
	1    1100 1200
	0    -1   -1   0   
$EndComp
Text GLabel 1100 800  1    60   Input ~ 0
PWA
Wire Wire Line
	1100 1000 1100 800 
Wire Wire Line
	1100 1400 1100 1600
Wire Wire Line
	1100 2000 1100 2200
Connection ~ 1100 1500
Wire Wire Line
	4300 1400 4300 1600
Connection ~ 4300 1500
$Comp
L R RI21
U 1 1 58071CE8
P 4300 1200
F 0 "RI21" H 4300 1300 60  0000 C CNN
F 1 "33K" H 4300 1100 60  0000 C CNN
F 2 "" H 4300 1200 60  0000 C CNN
F 3 "" H 4300 1200 60  0000 C CNN
	1    4300 1200
	0    -1   -1   0   
$EndComp
$Comp
L R RI22
U 1 1 58071D8D
P 4300 1800
F 0 "RI22" H 4300 1900 60  0000 C CNN
F 1 "2.2K" H 4300 1700 60  0000 C CNN
F 2 "" H 4300 1800 60  0000 C CNN
F 3 "" H 4300 1800 60  0000 C CNN
	1    4300 1800
	0    -1   -1   0   
$EndComp
Text GLabel 4300 800  1    60   Input ~ 0
PWA
Wire Wire Line
	4300 1000 4300 800 
Wire Wire Line
	4300 2000 4300 2200
Text GLabel 1800 1500 1    60   Input ~ 0
BID
$EndSCHEMATC
