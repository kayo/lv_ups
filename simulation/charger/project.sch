EESchema Schematic File Version 2
LIBS:spice
LIBS:project-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 12100 4300 0    60   ~ 0
-+pspice\n* \n.control\n  option nopage\n  tran 20nS 500uS 0uS\n  let Psw = vs1#branch*v(swd,sws)\n  let Pbr = vs2#branch*v(swd)\n  let Ibt = v(BTM)/0.05\n  let Pbt = Ibt^2*0.05\n  let Ig = vs3#branch\n  let Ip = vs4#branch\n  let It = vs5#branch\n  set gnuplot_terminal=png\n  gnuplot project\n*  + v(pwr)\n*  + v(pwm)\n*  + v(mod)\n*  + v(tg)\n*  + v(swg)\n*  + Ig\n*  + Ip*1e3\n*  + It*1e3\n  + v(swd)\n  + v(bat)\n*  + Psw\n*  + Pbr\n  + Ibt\n  + v(btm)\n  + v(iss)\n  + v(uss)\n  meas tran Psw_avg avg Psw from=200uS\n  meas tran Pbr_avg avg Pbr from=200uS\n  meas tran Pbt_avg avg Pbt from=200uS\n  meas tran Ibt_avg avg Ibt from=200uS\n  meas tran Ip_avg avg Ip\n  meas tran Ip_max max Ip\n  meas tran Ip_min min Ip\n  meas tran Ig_avg avg Ig\n  meas tran Ig_min min Ig\n  meas tran Ig_max max Ig\n.endc
$Comp
L V V1
U 1 1 572C2687
P 2800 3800
F 0 "V1" H 2700 4000 60  0000 C CNN
F 1 "DC 5V" H 2950 3600 47  0000 C CNN
F 2 "" H 2800 3800 60  0000 C CNN
F 3 "" H 2800 3800 60  0000 C CNN
	1    2800 3800
	1    0    0    -1  
$EndComp
$Comp
L 0 #GND01
U 1 1 572C3390
P 2800 4200
F 0 "#GND01" H 2800 4100 40  0001 C CNN
F 1 "0" H 2800 4130 40  0000 C CNN
F 2 "" H 2800 4200 60  0000 C CNN
F 3 "" H 2800 4200 60  0000 C CNN
	1    2800 4200
	1    0    0    -1  
$EndComp
Text GLabel 2800 2600 1    60   Input ~ 0
PWR
$Comp
L V VC2
U 1 1 5801CD21
P 2400 6900
F 0 "VC2" H 2300 7100 60  0000 C CNN
F 1 "DC 0V PULSE 0V 3.3V 0uS 10nS 10nS {d/f} {1/f}" H 2400 6300 47  0000 C CNN
F 2 "" H 2400 6900 60  0000 C CNN
F 3 "" H 2400 6900 60  0000 C CNN
	1    2400 6900
	1    0    0    -1  
$EndComp
$Comp
L 0 #GND02
U 1 1 5801D2BC
P 2400 7300
F 0 "#GND02" H 2400 7200 40  0001 C CNN
F 1 "0" H 2400 7230 40  0000 C CNN
F 2 "" H 2400 7300 60  0000 C CNN
F 3 "" H 2400 7300 60  0000 C CNN
	1    2400 7300
	1    0    0    -1  
$EndComp
Text GLabel 4600 1600 0    60   Input ~ 0
PWR
Text Notes 3300 2450 0    60   ~ 0
-pspice\n* \n.model uC_OD sw\n+ vt=0.5V\n+ vh=5mV\n+ ron=50m\n+ roff=1Meg\n\n.model HS sw\n+ vt=0.5V\n+ vh=1mV\n+ ron=5m\n+ roff=1Meg
$Comp
L PMOS XQ1
U 1 1 5801D791
P 6000 1700
F 0 "XQ1" V 6350 1750 50  0000 R CNN
F 1 "AO3407" V 6250 1850 50  0000 R CNN
F 2 "" H 6200 1800 50  0000 C CNN
F 3 "" H 6000 1700 50  0000 C CNN
	1    6000 1700
	0    1    -1   0   
$EndComp
$Comp
L DZ XD1
U 1 1 5801D935
P 6600 1900
F 0 "XD1" H 6600 2025 60  0000 C CNN
F 1 "SS14" H 6600 1775 60  0000 C CNN
F 2 "" H 6600 1900 60  0000 C CNN
F 3 "" H 6600 1900 60  0000 C CNN
	1    6600 1900
	0    -1   -1   0   
$EndComp
$Comp
L L L1
U 1 1 5801DA89
P 7000 1600
F 0 "L1" H 7000 1700 60  0000 C CNN
F 1 "30uH" H 7000 1550 60  0000 C CNN
F 2 "" H 7000 1600 60  0000 C CNN
F 3 "" H 7000 1600 60  0000 C CNN
	1    7000 1600
	1    0    0    -1  
$EndComp
$Comp
L 0 #GND03
U 1 1 5801DC00
P 6600 2800
F 0 "#GND03" H 6600 2700 40  0001 C CNN
F 1 "0" H 6600 2730 40  0000 C CNN
F 2 "" H 6600 2800 60  0000 C CNN
F 3 "" H 6600 2800 60  0000 C CNN
	1    6600 2800
	1    0    0    -1  
$EndComp
$Comp
L R RB1
U 1 1 5801E066
P 8400 2200
F 0 "RB1" H 8400 2300 60  0000 C CNN
F 1 "154m" H 8400 2100 60  0000 C CNN
F 2 "" H 8400 2200 60  0000 C CNN
F 3 "" H 8400 2200 60  0000 C CNN
	1    8400 2200
	0    -1   -1   0   
$EndComp
$Comp
L 0 #GND04
U 1 1 5801E24A
P 8400 3800
F 0 "#GND04" H 8400 3700 40  0001 C CNN
F 1 "0" H 8400 3730 40  0000 C CNN
F 2 "" H 8400 3800 60  0000 C CNN
F 3 "" H 8400 3800 60  0000 C CNN
	1    8400 3800
	1    0    0    -1  
$EndComp
Text GLabel 7500 1600 2    60   Input ~ 0
BAT
Text Notes 5500 1100 0    60   ~ 0
-pspice\n* \n.include ../.spice/dio.lib\n.include ../.spice/bjt.lib\n.include ../.spice/mos.lib\n.include ../.spice/amp.lib
$Comp
L C C1
U 1 1 5801EAA9
P 7400 2500
F 0 "C1" H 7400 2625 60  0000 C CNN
F 1 "100uF" H 7400 2375 60  0000 C CNN
F 2 "" H 6750 3050 60  0000 C CNN
F 3 "" H 6750 3050 60  0000 C CNN
	1    7400 2500
	0    -1   -1   0   
$EndComp
Text GLabel 8400 1800 1    60   Input ~ 0
BAT
$Comp
L R RC1
U 1 1 5801EBEC
P 7400 2000
F 0 "RC1" H 7400 2100 60  0000 C CNN
F 1 "10m" H 7400 1900 60  0000 C CNN
F 2 "" H 7400 2000 60  0000 C CNN
F 3 "" H 7400 2000 60  0000 C CNN
	1    7400 2000
	0    -1   -1   0   
$EndComp
$Comp
L 0 #GND05
U 1 1 5801ED8B
P 7400 2800
F 0 "#GND05" H 7400 2700 40  0001 C CNN
F 1 "0" H 7400 2730 40  0000 C CNN
F 2 "" H 7400 2800 60  0000 C CNN
F 3 "" H 7400 2800 60  0000 C CNN
	1    7400 2800
	1    0    0    -1  
$EndComp
Text GLabel 6400 1600 1    60   Input ~ 0
SWD
Text GLabel 6000 2100 3    60   Input ~ 0
SWG
Text GLabel 5600 1600 1    60   Input ~ 0
SWS
$Comp
L V VS1
U 1 1 58021E67
P 5000 1600
F 0 "VS1" V 4800 1600 60  0000 C CNN
F 1 "DC 0V" V 5200 1700 47  0000 R CNN
F 2 "" H 5000 1600 60  0000 C CNN
F 3 "" H 5000 1600 60  0000 C CNN
	1    5000 1600
	0    1    1    0   
$EndComp
$Comp
L V VS2
U 1 1 58022302
P 6600 2400
F 0 "VS2" V 6400 2400 60  0000 C CNN
F 1 "DC 0V" V 6800 2500 47  0000 R CNN
F 2 "" H 6600 2400 60  0000 C CNN
F 3 "" H 6600 2400 60  0000 C CNN
	1    6600 2400
	1    0    0    -1  
$EndComp
$Comp
L R RS1
U 1 1 58025071
P 8400 3400
F 0 "RS1" H 8400 3500 60  0000 C CNN
F 1 "{Rsh}" H 8400 3300 60  0000 C CNN
F 2 "" H 8400 3400 60  0000 C CNN
F 3 "" H 8400 3400 60  0000 C CNN
	1    8400 3400
	0    -1   -1   0   
$EndComp
Text GLabel 8400 3100 2    60   Input ~ 0
BSH
Text Notes 1950 1700 0    60   ~ 0
-pspice\n* \n.param\n+ f=200kHz\n+ d=0.90\n+ Rsh=0.05
$Comp
L V VB1
U 1 1 5802686F
P 8400 2800
F 0 "VB1" V 8200 2800 60  0000 C CNN
F 1 "4.24V" V 8600 2850 47  0000 R CNN
F 2 "" H 8400 2800 60  0000 C CNN
F 3 "" H 8400 2800 60  0000 C CNN
	1    8400 2800
	1    0    0    -1  
$EndComp
Text GLabel 7600 6100 2    60   Input ~ 0
SWG
Text GLabel 4700 7300 0    60   Input ~ 0
MOD
$Comp
L SWITCH S1
U 1 1 58032358
P 3300 5750
F 0 "S1" V 3100 5600 60  0000 C CNN
F 1 "uC_OD" H 3300 5900 60  0000 C CNN
F 2 "" H 3300 5400 60  0000 C CNN
F 3 "" H 3300 5400 60  0000 C CNN
	1    3300 5750
	0    1    1    0   
$EndComp
Text GLabel 2200 5700 0    60   Input ~ 0
PWM
Text GLabel 3300 5300 1    60   Input ~ 0
MOD
$Comp
L V VS3
U 1 1 58033D3D
P 7200 6100
F 0 "VS3" V 7000 6100 60  0000 C CNN
F 1 "DC 0V" V 7400 6200 47  0000 R CNN
F 2 "" H 7200 6100 60  0000 C CNN
F 3 "" H 7200 6100 60  0000 C CNN
	1    7200 6100
	0    1    1    0   
$EndComp
Text GLabel 6400 5300 1    60   Input ~ 0
PWR
$Comp
L R R2
U 1 1 58023743
P 5500 5300
F 0 "R2" H 5500 5400 60  0000 C CNN
F 1 "1K" H 5500 5200 60  0000 C CNN
F 2 "" H 5500 5300 60  0000 C CNN
F 3 "" H 5500 5300 60  0000 C CNN
	1    5500 5300
	0    -1   -1   0   
$EndComp
$Comp
L V VS4
U 1 1 58035B1B
P 5100 7300
F 0 "VS4" V 4900 7300 60  0000 C CNN
F 1 "DC 0V" V 5300 7400 47  0000 R CNN
F 2 "" H 5100 7300 60  0000 C CNN
F 3 "" H 5100 7300 60  0000 C CNN
	1    5100 7300
	0    1    1    0   
$EndComp
$Comp
L 0 #GND06
U 1 1 58036B28
P 3300 6500
F 0 "#GND06" H 3300 6400 40  0001 C CNN
F 1 "0" H 3300 6430 40  0000 C CNN
F 2 "" H 3300 6500 60  0000 C CNN
F 3 "" H 3300 6500 60  0000 C CNN
	1    3300 6500
	1    0    0    -1  
$EndComp
$Comp
L R R1
U 1 1 58036C47
P 5500 6900
F 0 "R1" H 5500 7000 60  0000 C CNN
F 1 "330" H 5500 6800 60  0000 C CNN
F 2 "" H 5500 6900 60  0000 C CNN
F 3 "" H 5500 6900 60  0000 C CNN
	1    5500 6900
	0    -1   1    0   
$EndComp
Text GLabel 5500 4900 1    60   Input ~ 0
PWR
$Comp
L QNPN Q1
U 1 1 58039C36
P 6300 5700
F 0 "Q1" H 6250 5900 60  0000 C CNN
F 1 "SS9013" H 6200 5500 60  0000 C CNN
F 2 "" H 5975 5700 60  0000 C CNN
F 3 "" H 5975 5700 60  0000 C CNN
	1    6300 5700
	1    0    0    -1  
$EndComp
$Comp
L V VS5
U 1 1 5803FCBC
P 5800 5700
F 0 "VS5" V 5600 5700 60  0000 C CNN
F 1 "DC 0V" V 6000 5800 47  0000 R CNN
F 2 "" H 5800 5700 60  0000 C CNN
F 3 "" H 5800 5700 60  0000 C CNN
	1    5800 5700
	0    1    1    0   
$EndComp
Text GLabel 5500 6100 0    60   Input ~ 0
TG
$Comp
L SWITCH S2
U 1 1 58047548
P 2800 3100
F 0 "S2" V 2600 2950 60  0000 C CNN
F 1 "HS" H 2800 3250 60  0000 C CNN
F 2 "" H 2800 2750 60  0000 C CNN
F 3 "" H 2800 2750 60  0000 C CNN
	1    2800 3100
	0    1    1    0   
$EndComp
$Comp
L 0 #GND07
U 1 1 580476F9
P 2300 3400
F 0 "#GND07" H 2300 3300 40  0001 C CNN
F 1 "0" H 2300 3330 40  0000 C CNN
F 2 "" H 2300 3400 60  0000 C CNN
F 3 "" H 2300 3400 60  0000 C CNN
	1    2300 3400
	1    0    0    -1  
$EndComp
$Comp
L V VC1
U 1 1 580478B6
P 1600 3500
F 0 "VC1" H 1500 3700 60  0000 C CNN
F 1 "DC 0V PWL 0uS 0V 10uS 0V 20uS 1V 350uS 1V 360uS 0V 500uS 0V" V 1050 3500 47  0000 C CNN
F 2 "" H 1600 3500 60  0000 C CNN
F 3 "" H 1600 3500 60  0000 C CNN
	1    1600 3500
	1    0    0    -1  
$EndComp
$Comp
L 0 #GND08
U 1 1 58047D89
P 1600 3900
F 0 "#GND08" H 1600 3800 40  0001 C CNN
F 1 "0" H 1600 3830 40  0000 C CNN
F 2 "" H 1600 3900 60  0000 C CNN
F 3 "" H 1600 3900 60  0000 C CNN
	1    1600 3900
	1    0    0    -1  
$EndComp
$Comp
L SWITCH S3
U 1 1 58048AC1
P 2400 6200
F 0 "S3" V 2200 6050 60  0000 C CNN
F 1 "HS" H 2400 6350 60  0000 C CNN
F 2 "" H 2400 5850 60  0000 C CNN
F 3 "" H 2400 5850 60  0000 C CNN
	1    2400 6200
	0    1    1    0   
$EndComp
$Comp
L 0 #GND09
U 1 1 58048C0B
P 1900 6500
F 0 "#GND09" H 1900 6400 40  0001 C CNN
F 1 "0" H 1900 6430 40  0000 C CNN
F 2 "" H 1900 6500 60  0000 C CNN
F 3 "" H 1900 6500 60  0000 C CNN
	1    1900 6500
	1    0    0    -1  
$EndComp
$Comp
L R R3
U 1 1 58049CF9
P 2700 6700
F 0 "R3" H 2700 6800 60  0000 C CNN
F 1 "10K" H 2700 6600 60  0000 C CNN
F 2 "" H 2700 6700 60  0000 C CNN
F 3 "" H 2700 6700 60  0000 C CNN
	1    2700 6700
	0    -1   1    0   
$EndComp
$Comp
L 0 #GND010
U 1 1 58049E69
P 2700 7100
F 0 "#GND010" H 2700 7000 40  0001 C CNN
F 1 "0" H 2700 7030 40  0000 C CNN
F 2 "" H 2700 7100 60  0000 C CNN
F 3 "" H 2700 7100 60  0000 C CNN
	1    2700 7100
	1    0    0    -1  
$EndComp
Text GLabel 1600 3050 0    60   Input ~ 0
PON
Text GLabel 1800 6150 0    60   Input ~ 0
PON
Text GLabel 3400 3000 1    60   Input ~ 0
PWR
$Comp
L C C2
U 1 1 5804A931
P 3400 3900
F 0 "C2" H 3400 4025 60  0000 C CNN
F 1 "100uF" H 3400 3775 60  0000 C CNN
F 2 "" H 2750 4450 60  0000 C CNN
F 3 "" H 2750 4450 60  0000 C CNN
	1    3400 3900
	0    -1   -1   0   
$EndComp
$Comp
L R RC2
U 1 1 5804A937
P 3400 3400
F 0 "RC2" H 3400 3500 60  0000 C CNN
F 1 "25m" H 3400 3300 60  0000 C CNN
F 2 "" H 3400 3400 60  0000 C CNN
F 3 "" H 3400 3400 60  0000 C CNN
	1    3400 3400
	0    -1   -1   0   
$EndComp
$Comp
L 0 #GND011
U 1 1 5804A93D
P 3400 4200
F 0 "#GND011" H 3400 4100 40  0001 C CNN
F 1 "0" H 3400 4130 40  0000 C CNN
F 2 "" H 3400 4200 60  0000 C CNN
F 3 "" H 3400 4200 60  0000 C CNN
	1    3400 4200
	1    0    0    -1  
$EndComp
Text GLabel 2800 3500 2    60   Input ~ 0
PWI
$Comp
L OA XO1
U 1 1 5804D4D8
P 6000 3600
F 0 "XO1" H 6100 3750 60  0000 C CNN
F 1 "LM224_ON" H 6000 2900 60  0000 C CNN
F 2 "" H 5900 3600 60  0000 C CNN
F 3 "" H 5900 3600 60  0000 C CNN
	1    6000 3600
	1    0    0    -1  
$EndComp
Text GLabel 5500 3500 0    60   Input ~ 0
BSH
$Comp
L 0 #GND012
U 1 1 5804DA67
P 6000 4100
F 0 "#GND012" H 6000 4000 40  0001 C CNN
F 1 "0" H 6000 4030 40  0000 C CNN
F 2 "" H 6000 4100 60  0000 C CNN
F 3 "" H 6000 4100 60  0000 C CNN
	1    6000 4100
	1    0    0    -1  
$EndComp
Text GLabel 6000 3100 1    60   Input ~ 0
PWA
$Comp
L R RI3
U 1 1 5804DC8B
P 6900 4000
F 0 "RI3" H 6900 4100 60  0000 C CNN
F 1 "100K" H 6900 3900 60  0000 C CNN
F 2 "" H 6900 4000 60  0000 C CNN
F 3 "" H 6900 4000 60  0000 C CNN
	1    6900 4000
	0    -1   -1   0   
$EndComp
$Comp
L 0 #GND013
U 1 1 5804DE39
P 6900 4400
F 0 "#GND013" H 6900 4300 40  0001 C CNN
F 1 "0" H 6900 4330 40  0000 C CNN
F 2 "" H 6900 4400 60  0000 C CNN
F 3 "" H 6900 4400 60  0000 C CNN
	1    6900 4400
	1    0    0    -1  
$EndComp
$Comp
L R RI2
U 1 1 5804E039
P 6500 4000
F 0 "RI2" H 6500 4100 60  0000 C CNN
F 1 "22K" H 6500 3900 60  0000 C CNN
F 2 "" H 6500 4000 60  0000 C CNN
F 3 "" H 6500 4000 60  0000 C CNN
	1    6500 4000
	0    -1   -1   0   
$EndComp
$Comp
L R RI1
U 1 1 5804E1AC
P 5100 4000
F 0 "RI1" H 5100 4100 60  0000 C CNN
F 1 "2.2K" H 5100 3900 60  0000 C CNN
F 2 "" H 5100 4000 60  0000 C CNN
F 3 "" H 5100 4000 60  0000 C CNN
	1    5100 4000
	0    -1   -1   0   
$EndComp
$Comp
L 0 #GND014
U 1 1 5804E3C4
P 5100 4400
F 0 "#GND014" H 5100 4300 40  0001 C CNN
F 1 "0" H 5100 4330 40  0000 C CNN
F 2 "" H 5100 4400 60  0000 C CNN
F 3 "" H 5100 4400 60  0000 C CNN
	1    5100 4400
	1    0    0    -1  
$EndComp
Text GLabel 7100 3600 2    60   Input ~ 0
ISS
Text Notes 7400 5650 0    60   ~ 0
/* Current amplifier */\nEQ[U[OUT]]: U[OUT] = R[SH] * I[IN] * (1 + R[2] / R[1]);\nEQ[R[2]]: solve(EQ[U[OUT]], R[2])[1];\nEQ[I[IN]]: solve(EQ[U[OUT]], I[IN])[1];\n\nPR[R[SH]]: R[SH]=50e-3$\nPR[I[IN]]: I[IN]=3.2$\nPR[R[1]]: R[1]=2.2e3$\nEQ[R[2]], PR[R[SH]], PR[I[IN]], PR[R[1]], U[OUT]=1.8;\nPR[R[2]]: R[2]=22e3$\nPR[U[OUT]]: EQ[U[OUT]], PR[R[SH]], PR[I[IN]], PR[R[1]], PR[R[2]];\nEQ[I[IN]], PR[R[SH]], PR[R[1]], PR[R[2]], U[OUT]=1.3;\nEQ[I[IN]], PR[R[SH]], PR[R[1]], PR[R[2]], U[OUT]=1.7;
Text Notes 8100 2700 1    60   ~ 0
BATTERY
Text GLabel 4300 3000 1    60   Input ~ 0
BAT
$Comp
L R RU2
U 1 1 5805198A
P 4300 3400
F 0 "RU2" H 4300 3500 60  0000 C CNN
F 1 "2K2" H 4300 3300 60  0000 C CNN
F 2 "" H 4300 3400 60  0000 C CNN
F 3 "" H 4300 3400 60  0000 C CNN
	1    4300 3400
	0    -1   -1   0   
$EndComp
$Comp
L R RU1
U 1 1 58051C75
P 4300 4000
F 0 "RU1" H 4300 4100 60  0000 C CNN
F 1 "3K3" H 4300 3900 60  0000 C CNN
F 2 "" H 4300 4000 60  0000 C CNN
F 3 "" H 4300 4000 60  0000 C CNN
	1    4300 4000
	0    -1   -1   0   
$EndComp
Text GLabel 4500 3700 2    60   Input ~ 0
USS
$Comp
L 0 #GND015
U 1 1 580520C0
P 4300 4400
F 0 "#GND015" H 4300 4300 40  0001 C CNN
F 1 "0" H 4300 4330 40  0000 C CNN
F 2 "" H 4300 4400 60  0000 C CNN
F 3 "" H 4300 4400 60  0000 C CNN
	1    4300 4400
	1    0    0    -1  
$EndComp
Text Notes 9000 4150 0    60   ~ 0
+pspice\n* \n.control\n  option nopage\n  tran 20nS 500uS 0uS\n  let Psw = vs1#branch*v(swd,sws)\n  let Pbr = vs2#branch*v(swd)\n  let Ibt = vb1#branch\n  let Psh = Ibt*v(bsh)\n  let Ig = vs3#branch\n  let Ip = vs4#branch\n  let It = vs5#branch\n  set gnuplot_terminal=png\n  gnuplot project\n  + v(pwr)\n  + v(pwm)\n*  + v(mod)\n  + v(tg)\n  + v(swg)\n*  + Ig\n*  + Ip*1e3\n*  + It*1e3\n  + v(swd)\n  + v(bat)\n  + Psw\n  + Pbr\n  + Ibt\n  + v(iss)\n  meas tran Psw_avg avg Psw from=200uS\n  meas tran Pbr_avg avg Pbr from=200uS\n  meas tran Psh_avg avg Psh from=200uS\n  meas tran Ibt_avg avg Ibt from=200uS\n  meas tran Ip_avg avg Ip\n  meas tran Ip_max max Ip\n  meas tran Ip_min min Ip\n  meas tran Ig_avg avg Ig\n  meas tran Ig_min min Ig\n  meas tran Ig_max max Ig\n.endc
Wire Wire Line
	2800 2600 2800 2800
Wire Wire Line
	2800 4000 2800 4200
Wire Wire Line
	2400 7100 2400 7300
Wire Wire Line
	6200 1600 6800 1600
Wire Wire Line
	6600 1600 6600 1800
Connection ~ 6600 1600
Wire Wire Line
	6600 2600 6600 2800
Wire Wire Line
	7200 1600 7500 1600
Wire Wire Line
	8400 1800 8400 2000
Wire Wire Line
	8400 3600 8400 3800
Wire Wire Line
	7400 1600 7400 1800
Connection ~ 7400 1600
Wire Wire Line
	7400 2200 7400 2400
Wire Wire Line
	7400 2600 7400 2800
Wire Wire Line
	5200 1600 5800 1600
Wire Wire Line
	4800 1600 4600 1600
Wire Wire Line
	6600 2000 6600 2200
Wire Wire Line
	8400 3000 8400 3200
Wire Wire Line
	8400 2400 8400 2600
Wire Wire Line
	6000 1900 6000 2100
Connection ~ 6400 6100
Wire Wire Line
	3300 6050 3300 6500
Wire Wire Line
	3000 5800 2800 5800
Wire Wire Line
	2800 5800 2800 6300
Wire Wire Line
	2800 6300 3300 6300
Connection ~ 3300 6300
Wire Wire Line
	2200 5700 3000 5700
Wire Wire Line
	2400 5900 2400 5700
Connection ~ 2400 5700
Wire Wire Line
	3300 5450 3300 5300
Wire Wire Line
	7400 6100 7600 6100
Wire Wire Line
	6400 5900 6400 6300
Wire Wire Line
	6400 5500 6400 5300
Wire Wire Line
	4900 7300 4700 7300
Connection ~ 5500 5700
Wire Wire Line
	5500 5100 5500 4900
Wire Wire Line
	6400 6100 7000 6100
Wire Wire Line
	5500 5500 5500 6700
Wire Wire Line
	6100 5700 6000 5700
Wire Wire Line
	5600 5700 5500 5700
Connection ~ 5500 6500
Wire Wire Line
	5600 6500 5500 6500
Wire Wire Line
	5500 7300 5300 7300
Wire Wire Line
	5500 7300 5500 7100
Wire Wire Line
	2800 3400 2800 3600
Wire Wire Line
	2500 3150 2300 3150
Wire Wire Line
	2300 3150 2300 3400
Wire Wire Line
	2500 3050 1600 3050
Wire Wire Line
	1600 3050 1600 3300
Wire Wire Line
	1600 3700 1600 3900
Wire Wire Line
	2400 6500 2400 6700
Wire Wire Line
	2100 6250 1900 6250
Wire Wire Line
	1900 6250 1900 6500
Wire Wire Line
	2100 6150 1800 6150
Wire Wire Line
	2700 5700 2700 6500
Connection ~ 2700 5700
Wire Wire Line
	2700 6900 2700 7100
Wire Wire Line
	3400 3600 3400 3800
Wire Wire Line
	3400 4000 3400 4200
Wire Wire Line
	3400 3000 3400 3200
Wire Wire Line
	5700 3500 5500 3500
Wire Wire Line
	5100 3700 5700 3700
Wire Wire Line
	6000 3900 6000 4100
Wire Wire Line
	6000 3300 6000 3100
Wire Wire Line
	6300 3600 7100 3600
Wire Wire Line
	6900 3600 6900 3800
Wire Wire Line
	6900 4200 6900 4400
Wire Wire Line
	6500 3600 6500 3800
Connection ~ 6500 3600
Wire Wire Line
	6500 4200 6500 4400
Wire Wire Line
	6500 4400 5500 4400
Wire Wire Line
	5500 4400 5500 3700
Wire Wire Line
	5100 3700 5100 3800
Connection ~ 5500 3700
Wire Wire Line
	5100 4200 5100 4400
Connection ~ 6900 3600
Wire Notes Line
	8700 2000 8700 3000
Wire Notes Line
	8700 3000 8100 3000
Wire Notes Line
	8100 3000 8100 2000
Wire Notes Line
	8100 2000 8700 2000
Wire Wire Line
	4300 3600 4300 3800
Wire Wire Line
	4300 3700 4500 3700
Connection ~ 4300 3700
Wire Wire Line
	4300 3000 4300 3200
Wire Wire Line
	4300 4200 4300 4400
Wire Wire Line
	6100 6500 6000 6500
Wire Wire Line
	6400 6700 6400 6900
$Comp
L V VS6
U 1 1 58040EC0
P 5800 6500
F 0 "VS6" V 5600 6500 60  0000 C CNN
F 1 "DC 0V" V 6000 6600 47  0000 R CNN
F 2 "" H 5800 6500 60  0000 C CNN
F 3 "" H 5800 6500 60  0000 C CNN
	1    5800 6500
	0    1    1    0   
$EndComp
$Comp
L 0 #GND016
U 1 1 58040D6E
P 6400 6900
F 0 "#GND016" H 6400 6800 40  0001 C CNN
F 1 "0" H 6400 6830 40  0000 C CNN
F 2 "" H 6400 6900 60  0000 C CNN
F 3 "" H 6400 6900 60  0000 C CNN
	1    6400 6900
	1    0    0    -1  
$EndComp
$Comp
L QPNP Q2
U 1 1 58040864
P 6300 6500
F 0 "Q2" H 6250 6700 60  0000 C CNN
F 1 "SS9012" H 6200 6300 60  0000 C CNN
F 2 "" H 5975 6500 60  0000 C CNN
F 3 "" H 5975 6500 60  0000 C CNN
	1    6300 6500
	1    0    0    1   
$EndComp
Text Notes 6500 6600 0    60   ~ 0
Opening\nnetwork
Text Notes 6500 5800 0    60   ~ 0
Closing\nnetwork
$Comp
L V V2
U 1 1 58068595
P 4300 5700
F 0 "V2" H 4200 5900 60  0000 C CNN
F 1 "DC 3.3V" H 4500 5500 47  0000 C CNN
F 2 "" H 4300 5700 60  0000 C CNN
F 3 "" H 4300 5700 60  0000 C CNN
	1    4300 5700
	1    0    0    -1  
$EndComp
Wire Wire Line
	4300 5900 4300 6100
Wire Wire Line
	4300 5500 4300 5300
Text GLabel 4300 5300 1    60   Input ~ 0
PWA
$Comp
L 0 #GND017
U 1 1 58068FD1
P 4300 6100
F 0 "#GND017" H 4300 6000 40  0001 C CNN
F 1 "0" H 4300 6030 40  0000 C CNN
F 2 "" H 4300 6100 60  0000 C CNN
F 3 "" H 4300 6100 60  0000 C CNN
	1    4300 6100
	1    0    0    -1  
$EndComp
$EndSCHEMATC
